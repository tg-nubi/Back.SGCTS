﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Text;

namespace Back.SGCTS.DTO.Base
{
    public class BaseResponseEntityDTO<TEntity> : BaseResponseDTO
    {
        [JsonProperty("data")]
        public TEntity Object { get; set; }
        
        public BaseResponseEntityDTO(TEntity entity)
        {
            Object = entity;
        }
    }
}
