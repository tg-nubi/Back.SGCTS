﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Text;

namespace Back.SGCTS.DTO.Base
{
    public class BaseResponseDTO
    {
        [JsonProperty("success")]
        public bool Success { get { return Errors.Count == 0; } private set { } }

        [JsonProperty("errors")]
        public IList<string> Errors { get; private set; } = new List<string>();

        public void AddError(string error)
        {
            Errors.Add(error);
        }
    }
}
