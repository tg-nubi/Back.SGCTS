﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace Back.SGCTS.DTO.Auth
{
    public class CreatePasswordDTO
    {
        [Required]
        public string User { get; set; }

        [Required]        
        public string CurrentPassword { get; set; }
        
        [Required]
        public string NewPassword { get; set; }
        
        [Required]
        public string ConfirmPassword { get; set; }
    }
}
