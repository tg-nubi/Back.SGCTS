﻿using Newtonsoft.Json;
using System.Collections.Generic;

namespace Back.SGCTS.DTO.Dashboard
{
    public class ExecutionSeriesDashboardDTO
    {
        [JsonProperty("name")]
        public string Name { get; set; }

        [JsonProperty("data")]
        public IEnumerable<int> Data { get; set; }
    }
}