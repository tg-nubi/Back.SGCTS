﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Text;

namespace Back.SGCTS.DTO.Dashboard
{
    public class GeneralDashboardDTO
    {
        [JsonProperty("title")]
        public string Title { get; set; }

        [JsonProperty("general")]
        public IDictionary<string,int> General { get; set; }

        [JsonProperty("execution")]
        public ExecutionDashboardDTO Execution { get; set; }
    }
}
