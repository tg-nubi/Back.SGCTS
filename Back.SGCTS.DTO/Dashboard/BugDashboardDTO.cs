﻿using Newtonsoft.Json;
using System.Collections.Generic;

namespace Back.SGCTS.DTO.Dashboard
{
    public class ExecutionDashboardDTO
    {
        [JsonProperty("series")]
        public IList<ExecutionSeriesDashboardDTO> Series { get; set; } = new List<ExecutionSeriesDashboardDTO>();

        [JsonProperty("date")]
        public IEnumerable<string> Date { get; set; }

    }
}