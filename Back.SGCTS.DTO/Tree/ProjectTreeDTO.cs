﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Text;

namespace Back.SGCTS.DTO.Tree
{
    public class ProjectTreeDTO : TreeBaseDTO
    {
        [JsonProperty("systemId")]
        public int SystemId { get; set; }
        public ProjectTreeDTO(string name) : base(name, "project")
        {
        }
    }
}
