﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Text;

namespace Back.SGCTS.DTO.Tree
{
    public class CaseTreeDTO : TreeBaseDTO
    {
        [JsonProperty("systemId")]
        public int SystemId { get; set; }
        [JsonProperty("projectId")]
        public int ProjectId { get; set; }
        [JsonProperty("scenarioId")]
        public int Scenario { get; set; }
        public CaseTreeDTO(string name) : base(name, "case")
        {
        }
    }
}
