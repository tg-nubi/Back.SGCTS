﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Back.SGCTS.DTO.Tree
{
    public class SystemTreeDTO : TreeBaseDTO
    {
        public SystemTreeDTO(string name) : base(name, "system" )
        {
        }
    }
}
