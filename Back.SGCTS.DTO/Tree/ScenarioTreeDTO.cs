﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Text;

namespace Back.SGCTS.DTO.Tree
{
    public class ScenarioTreeDTO : TreeBaseDTO
    {
        [JsonProperty("systemId")]
        public int SystemId { get; set; }
        [JsonProperty("projectId")]
        public int ProjectId { get; set; }
        public ScenarioTreeDTO(string name) : base(name, "scenario")
        {
        }
    }
}
