﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Text;

namespace Back.SGCTS.DTO.Tree
{
    public abstract class TreeBaseDTO
    {
        [JsonProperty("id")]
        public int Id { get; set; }
        [JsonProperty("name")]
        public string Name { get; set; }
        [JsonProperty("type")]
        public string Type { get; set; }
        [JsonProperty("children")]
        public IEnumerable<TreeBaseDTO> Children { get; set; }
        [JsonProperty("statusType")]
        public string StatusType { get; set; }
        [JsonProperty("key")]
        public string key { get { return $"{Type}{Id}"; } private set { } }
        protected TreeBaseDTO(string name, string type)
        {
            Name = name;
            Type = type;
        }
    }
}
