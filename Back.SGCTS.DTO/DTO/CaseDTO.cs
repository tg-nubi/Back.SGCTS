﻿using System;
using System.Collections.Generic;
using System.Text;
using System.ComponentModel.DataAnnotations;
using Newtonsoft.Json;

namespace Back.SGCTS.DTO.DTO
{
    public class CaseDTO
    {
        [JsonProperty("id")]
        public int Id { get; set; }
        
        [Required]
        [JsonProperty("name")]
        public string Name { get; set; }
        
        [Required]
        [JsonProperty("preCondition")]
        public string PreCondition { get; set; }

        [Required]
        [JsonProperty("dataMass")]
        public string DataMass { get; set; }

        [Required]
        [JsonProperty("resultExpected")]
        public string ResultExpected { get; set; }

        [Required]
        [JsonProperty("scenarioId")]
        public int ScenarioId { get; set; }

        [Required]
        [JsonProperty("statusType")]
        public string StatusType { get; set; }

        [Required]
        [JsonProperty("userId")]
        public int UserId { get; set; }

        [JsonProperty("reasonBlock")]
        public string ReasonBlock { get; set; }
    }
}
