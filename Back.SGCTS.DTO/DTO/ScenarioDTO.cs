﻿using System;
using System.Collections.Generic;
using System.Text;
using System.ComponentModel.DataAnnotations;
using Newtonsoft.Json;

namespace Back.SGCTS.DTO.DTO
{
    public class ScenarioDTO
    {
        [JsonProperty("id")]
        public int Id { get; set; }
        [Required]
        [JsonProperty("name")]
        public string Name { get; set; }

        [JsonProperty("description")]
        public string Description { get; set; }
        
        [Required]
        [JsonProperty("projectId")]
        public int ProjectId { get; set; }

        [Required]
        [JsonProperty("statusType")]
        public string StatusType { get; set; }

    }
}
