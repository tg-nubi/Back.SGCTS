﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace Back.SGCTS.DTO.DTO
{
    public class ProjectDTO
    {
        [JsonProperty("id")]
        public int Id { get; set; }
        
        [Required]
        [JsonProperty("name")]
        public string Name { get; set; }
        
        [JsonProperty("goal")]
        public string Goal { get; set; }
        
        [JsonProperty("statusType")]
        public string StatusType { get; set; }

        [JsonProperty("usersIds")]
        public IList<int> UsersIds { get; set; }

        [JsonProperty("systemsIds")]
        public IList<int> SystemsIds { get; set; }
    }
}
