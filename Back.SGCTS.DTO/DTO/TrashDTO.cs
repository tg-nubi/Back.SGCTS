﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Text;

namespace Back.SGCTS.DTO.DTO
{
    public class TrashDTO
    {
        [JsonProperty("type")]
        public string Type { get; set; }
        [JsonProperty("name")]
        public string Name { get; set; }
        [JsonProperty("id")]
        public int Id { get; set; }
        [JsonProperty("objectId")]
        public int ObjectId { get; set; }
        [JsonProperty("userName")]
        public string UserName { get; set; }
    }
}
