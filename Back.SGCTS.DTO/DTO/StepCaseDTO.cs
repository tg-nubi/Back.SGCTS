﻿using System;
using System.Collections.Generic;
using System.Text;
using System.ComponentModel.DataAnnotations;
using Newtonsoft.Json;

namespace Back.SGCTS.DTO.DTO
{
    public class StepCaseDTO
    {
        [JsonProperty("id")]
        public int Id { get; set; }

        [Required]
        [JsonProperty("stepNumber")]
        public string NumberStep { get; set; }

        [JsonProperty("action")]
        public string Action { get; set; }

        [JsonProperty("resultExpected")]
        public string ResultExpected { get; set; }

        [JsonProperty("caseId")]
        public int CaseId { get; set; }
    }
}
