﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Text;

namespace Back.SGCTS.DTO.DTO
{
    public class ExecutionDTO
    {
        [JsonProperty("id")]
        public int Id { get; set; }

        [JsonProperty("userId")]
        public int UserId { get; set; }

        [JsonProperty("userName")]
        public string UserName { get; set; }

        [JsonProperty("caseId")]
        public int CaseId { get; set; }
        
        [JsonProperty("statusType")]
        public string StatusType { get; set; }

        [JsonProperty("note")]
        public string Note { get; set; }

        [JsonProperty("attachment")]
        public string Attachment { get; set; }

        [JsonProperty("dateStart")]
        public DateTime DateStart { get; set; }

        [JsonProperty("dateEnd")]
        public DateTime DateEnd { get; set; }

        [JsonProperty("dateShow")]
        public string DateShow { get; set; }
    }
}
