﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace Back.SGCTS.DTO.DTO
{
    public class AuthDTO
    {
        [Required]
        [JsonProperty("user")]
        public string User { get; set; }

        [Required]
        [JsonProperty("password")] 
        public string Password { get; set; }
    }
}
