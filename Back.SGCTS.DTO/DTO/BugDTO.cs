﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace Back.SGCTS.DTO.DTO
{
    public class BugDTO
    {
        [JsonProperty("id")]
        public int Id { get; set; }

        [JsonProperty("description")]
        public string Description { get; set; }
        
        [Required]
        [JsonProperty("title")]
        public string Title { get; set; }
        
        [JsonProperty("severity")]
        public int Severity { get; set; }

        [JsonProperty("action")]
        public string ActionStepBug { get; set; }
        
        [JsonProperty("statusType")]
        public string StatusType { get; set; }
        
        [JsonProperty("executionId")]
        public int ExecutionId { get; set; }

        [JsonProperty("priority")]
        public string Priority { get; set; }

        [JsonProperty("userId")]
        public int UserId { get; set; }

        [JsonProperty("keywords")]
        public string Keywords { get; set; }
        
        [JsonProperty("keywordsList")]
        public IEnumerable<string> KeywordsList { get { return Keywords?.Split(" - "); } set { Keywords = String.Join(" - ", value); } }

        [JsonProperty("upload")]
        public string Attachment { get; set; }

    }
}
