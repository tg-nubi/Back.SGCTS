﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace Back.SGCTS.DTO.DTO
{
    public class UserDTO
    {
        [JsonProperty("id")]
        public int Id { get; set; }
        [JsonProperty("permissionType")]
        public string PermissionType { get; set; }
        
        [Required]
        [JsonProperty("name")]
        public string Name { get; set; }

        [Required]
        [JsonProperty("lastName")]
        public string LastName { get; set; }

        [Required]
        [JsonProperty("email")]
        public string Email { get; set; }

        [JsonProperty("login")]
        public string Login { get; set; }
        
        [JsonProperty("registration")]
        public string Registration { get; set; }
        
        [JsonProperty("statusType")]
        public string StatusType { get; set; }
    }
}
