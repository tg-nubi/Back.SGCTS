﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Text;

namespace Back.SGCTS.DTO.DTO
{
    public class AuthResponseDTO
    {
        [JsonProperty("token")]
        public string Token { get; set; }
        
        [JsonProperty("expiration")]
        public DateTime Expiration { get; set; }
        
        [JsonProperty("name")]
        public string Name { get; set; }

        [JsonProperty("login")]
        public string Login { get; set; }

        [JsonProperty("resetPasswordRequired")]
        public bool ResetPasswordRequired { get; set; }

        [JsonProperty("permission")]
        public string Permission { get; set; }
    }
}
