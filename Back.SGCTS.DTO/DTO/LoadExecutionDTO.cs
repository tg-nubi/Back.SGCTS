﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Text;

namespace Back.SGCTS.DTO.DTO
{
    public class LoadExecutionDTO
    {
        [JsonProperty("system")]
        public string System { get; set; }

        [JsonProperty("project")]
        public string Project { get; set; }

        [JsonProperty("scenario")]
        public string Scenario { get; set; }

        [JsonProperty("case")]
        public string Case { get; set; }

        [JsonProperty("caseId")]
        public int CaseId { get; set; }

        [JsonProperty("steps")]
        public List<StepCaseDTO> StepCase { get; set; }

        [JsonProperty("dateStart")]
        public DateTime DateStart { get; set; } = DateTime.Now;
    }
}
