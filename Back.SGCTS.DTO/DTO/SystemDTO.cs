﻿using System;
using System.Collections.Generic;
using System.Text;
using System.ComponentModel.DataAnnotations;
using Newtonsoft.Json;

namespace Back.SGCTS.DTO.DTO
{
    public class SystemDTO
    {
        [JsonProperty("id")]
        public int Id { get; set; }
        [Required]
        [JsonProperty("name")]
        public string Name { get; set; }

        [Required]
        [JsonProperty("initials")]
        public string Initials { get; set; }

        [Required]
        [JsonProperty("statusType")]
        public string StatusType { get; set; }
    }
}
