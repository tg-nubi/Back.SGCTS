﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Text;

namespace Back.SGCTS.DTO.DTO
{
    public class CaseResponseDTO: CaseDTO
    {
        [JsonProperty("scenarioName")]
        public string ScenarioName { get; set; }
    }
}
