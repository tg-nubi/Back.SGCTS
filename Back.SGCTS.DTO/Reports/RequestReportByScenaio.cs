﻿namespace Back.SGCTS.DTO.Reports
{
    public class RequestReportByScenaio : RequestReportBase
    {
        public RequestReportByScenaio(int systemId, int projectId, int scenarioId) : base(systemId, projectId, scenarioId)
        {
        }
    }
}
