﻿namespace Back.SGCTS.DTO.Reports
{
    public class RequestReportByProject : RequestReportBase
    {
        public RequestReportByProject(int systemId, int projectId) : base(systemId, projectId, null)
        {
        }
    }
}
