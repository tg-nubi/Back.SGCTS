﻿  namespace Back.SGCTS.DTO.Reports
{
    public abstract class RequestReportBase
    {
        public int SystemId { get; set; }
        public int? ProjectId { get; set; }
        public int? ScenarioId { get; set; }

        public RequestReportBase(int systemId, int? projectId, int? scenarioId)
        {
            SystemId = systemId;
            ProjectId = projectId;
            ScenarioId = scenarioId;
        }
    }
}
