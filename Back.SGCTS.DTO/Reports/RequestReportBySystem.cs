﻿namespace Back.SGCTS.DTO.Reports
{
    public class RequestReportBySystem : RequestReportBase
    {
        public RequestReportBySystem(int systemId) : base(systemId, null, null)
        {
        }
    }
}
