﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Back.SGCTS.DTO.Exports
{
    public class RequestDataExport
    {
        public int? SystemId { get; set; }
        public int? ProjectId { get; set; }
        public int? ScenarioId { get; set; }

        public RequestDataExport(int? systemId, int? projectId, int? scenarioId)
        {
            SystemId = systemId;
            ProjectId = projectId;
            ScenarioId = scenarioId;
        }
    }
}
