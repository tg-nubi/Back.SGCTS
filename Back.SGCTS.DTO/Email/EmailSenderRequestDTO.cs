﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Text;

namespace Back.SGCTS.DTO.Email
{
    public class EmailSenderRequestDTO
    {
        [JsonProperty("api_user")]
        public string User { get; set; }

        [JsonProperty("api_key")]
        public string Key { get; set; }

        [JsonProperty("subject")]
        public string Subject { get; set; }

        [JsonProperty("html")]
        public string Html { get; set; }

        [JsonProperty("to")]
        public IEnumerable<EmailUsersDTO> To { get; set; }

        [JsonProperty("from")]
        public EmailUsersDTO From { get; set; }
    }
}
