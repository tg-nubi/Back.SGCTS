﻿using System;
using System.Collections.Generic;
using System.Security.Cryptography;
using System.Text;

namespace Back.SGCTS.ApplicationService.Extension
{
    public static class Cript
    {
        public static string GetHashSha1(this string value)
        {
            byte[] data = Encoding.Default.GetBytes(value);
            SHA1 sha = new SHA1CryptoServiceProvider();
            var result = sha.ComputeHash(data);
            return BitConverter.ToString(result).Replace("-", "").ToLower();
        }
    }
}
