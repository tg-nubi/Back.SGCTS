﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Reflection;
using System.Text;

namespace Back.SGCTS.ApplicationService.Extension
{
    public static class EnumExtension
    {
        public static string GetDescription(this System.Enum StatusTypeEnum)
        {
            FieldInfo fieldInfo = StatusTypeEnum.GetType().GetField(StatusTypeEnum.ToString());
            if (fieldInfo == null)
                return null;
            var attribute = (DescriptionAttribute)fieldInfo.GetCustomAttribute(typeof(DescriptionAttribute));

            return attribute?.Description;
        }


        public static TEnum GetEnumValueFromDescription<TEnum>(this string enumDescription)
        {
            var type = typeof(TEnum);
            enumDescription = enumDescription.ToLower();
            if (!type.IsEnum) throw new InvalidOperationException();
            foreach (var field in type.GetFields())
            {
                if (Attribute.GetCustomAttribute(field,
                    typeof(DescriptionAttribute)) is DescriptionAttribute attribute)
                {
                    if (attribute.Description.ToLower() == enumDescription)
                        return (TEnum)field.GetValue(null);
                }
                else
                {
                    if (field.Name.ToLower() == enumDescription)
                        return (TEnum)field.GetValue(null);
                }
            }
            throw new ArgumentException("Not found.", "enum description");
        }
        public static bool TryGetEnumValueFromDescription<TEnum>(this string enumDescription, out TEnum returnEnum)
        {
            returnEnum = default(TEnum);
            var type = typeof(TEnum);
            if (String.IsNullOrEmpty(enumDescription))
                return false;
            enumDescription = enumDescription.ToLower();
            if (!type.IsEnum) throw new InvalidOperationException();
            foreach (var field in type.GetFields())
            {
                if (Attribute.GetCustomAttribute(field,
                    typeof(DescriptionAttribute)) is DescriptionAttribute attribute)
                {
                    if (attribute.Description.ToLower() == enumDescription)
                    {
                        returnEnum = (TEnum)field.GetValue(null);
                        return true;
                    }
                }
                else
                {
                    if (field.Name.ToLower() == enumDescription)
                    {
                        returnEnum = (TEnum)field.GetValue(null);
                        return true;
                    }
                }
            }
            return false;
        }

    }
}
