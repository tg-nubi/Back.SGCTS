﻿using Back.SGCTS.Domain.Domain;
using Back.SGCTS.Domain.Interface.Services;
using Back.SGCTS.DTO.Reports;
using System.Collections.Generic;

namespace Back.SGCTS.ApplicationService.Factories.Reports
{
    public class ReportByProject : ReportService
    {
        public ReportByProject(ISystemAppService systemAppService,
                              IProjectAppService projectAppService,
                              IScenarioAppService scenarioAppService,
                              ICaseAppService caseAppService,
                              IUserAppService userAppService) : base(systemAppService, projectAppService, scenarioAppService, caseAppService, userAppService)
        {
        }

        public override Domain.Domain.System GetReport(RequestReportBase requestReport)
        {
            var project = _projectAppService.GetById((int)requestReport.ProjectId);
            var system = _systemAppService.GetById(requestReport.SystemId);

            project.Scenarios = _scenarioAppService.GetFullByProjectId(project.Id);
            system.Projects = new List<Project>() { project };

            foreach (var scenarios in project.Scenarios)
            {
                foreach (var cases in scenarios.Cases)
                {
                    cases.User = _userAppService.GetById(cases.UserId);
                }
            }

            return system;
        }
    }
}
