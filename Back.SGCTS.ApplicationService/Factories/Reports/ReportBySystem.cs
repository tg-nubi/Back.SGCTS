﻿using Back.SGCTS.Domain.Interface.Repository;
using Back.SGCTS.Domain.Interface.Services;
using Back.SGCTS.DTO.Reports;

namespace Back.SGCTS.ApplicationService.Factories.Reports
{
    public class ReportBySystem : ReportService
    {
        private readonly IProjectRepository _projectRepository;
        public ReportBySystem(ISystemAppService systemAppService,
                              IProjectAppService projectAppService,
                              IScenarioAppService scenarioAppService,
                              ICaseAppService caseAppService,
                              IProjectRepository projectRepository,
                              IUserAppService userAppService) : base(systemAppService, projectAppService, scenarioAppService, caseAppService, userAppService)
        {
            _projectRepository = projectRepository;
        }

        public override Domain.Domain.System GetReport(RequestReportBase requestReport)
        {
            var system = _systemAppService.GetById(requestReport.SystemId);

            system.Projects = _projectRepository.GetProjectsBySystemId(system.Id);

            foreach (var project in system.Projects)
            {
                project.Scenarios = _scenarioAppService.GetFullByProjectId(project.Id);

                foreach (var scenarios in project.Scenarios)
                {
                    foreach (var cases in scenarios.Cases)
                    {
                        cases.User = _userAppService.GetById(cases.UserId);
                    }
                }
            }

            return system;
        }
    }
}
