﻿using Back.SGCTS.Domain.Interface.Services;
using Back.SGCTS.DTO.Reports;

namespace Back.SGCTS.ApplicationService.Factories.Reports
{
    public abstract class ReportService : IReportService
    {
        protected readonly ISystemAppService _systemAppService;
        protected readonly IProjectAppService _projectAppService;
        protected readonly IScenarioAppService _scenarioAppService;
        protected readonly ICaseAppService _caseAppService;
        protected readonly IUserAppService _userAppService;

        public ReportService(ISystemAppService systemAppService,
                                     IProjectAppService projectAppService,
                                     IScenarioAppService scenarioAppService,
                                     ICaseAppService caseAppService,
                                     IUserAppService userAppService)
        {
            _systemAppService = systemAppService;
            _projectAppService = projectAppService;
            _scenarioAppService = scenarioAppService;
            _caseAppService = caseAppService;
            _userAppService = userAppService;
        }

        public abstract Domain.Domain.System GetReport(RequestReportBase requestReport);
    }
}
