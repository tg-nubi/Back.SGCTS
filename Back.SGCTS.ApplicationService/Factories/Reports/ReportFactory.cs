﻿using Back.SGCTS.DTO.Reports;
using System;

namespace Back.SGCTS.ApplicationService.Factories.Reports
{
    public class ReportFactory
    {
        private readonly ReportByProject _reportByProject;
        private readonly ReportByScenario _reportByScenario;
        private readonly ReportBySystem _reportBySystem;

        public ReportFactory(ReportByProject reportByProject, ReportByScenario reportByScenario, ReportBySystem reportBySystem)
        {
            _reportByProject = reportByProject;
            _reportByScenario = reportByScenario;
            _reportBySystem = reportBySystem;
        }

        public IReportService GetInstance(RequestReportBase requestReport)
        {
            switch (requestReport)
            {
                case RequestReportByScenaio _:
                    return _reportByScenario;
                case RequestReportByProject _:
                    return _reportByProject;
                case RequestReportBySystem _:
                    return _reportBySystem;
                default:
                    throw new NotImplementedException();
            }
        }
    }
}
