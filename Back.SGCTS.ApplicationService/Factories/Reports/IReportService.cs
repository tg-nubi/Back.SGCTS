﻿using Back.SGCTS.DTO.Reports;

namespace Back.SGCTS.ApplicationService.Factories.Reports
{
    public interface IReportService
    {
        Domain.Domain.System GetReport(RequestReportBase request);
    }
}