﻿using Back.SGCTS.DTO.Dashboard;
using System;
using System.Collections.Generic;
using System.Text;

namespace Back.SGCTS.ApplicationService.Interfaces
{
    public interface IDashboardApplicationService
    {
        IList<GeneralDashboardDTO> GetGeneralDashboard(int userId);
    }
}
