﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Back.SGCTS.ApplicationService.Interfaces
{
    public interface ITreeApplicationService
    {
        IList<Domain.Domain.System> Get(int userId);
    }
}
