﻿using Back.SGCTS.ApplicationService.Model;
using System;
using System.Collections.Generic;
using System.Text;

namespace Back.SGCTS.ApplicationService.Interfaces
{
    public interface IEmailService
    {
        void SendEmail(Message message);
    }
}
