﻿using Back.SGCTS.DTO.Reports;
using iTextSharp.text;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Text;

namespace Back.SGCTS.ApplicationService.Interfaces
{
    public interface IReportApplicationServices
    {
        FileStreamResult GenerateReport(RequestReportBase requestReportBase);
    }
}