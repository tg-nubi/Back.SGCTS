﻿using Back.SGCTS.DTO.DTO;
using System;
using System.Collections.Generic;
using System.Text;

namespace Back.SGCTS.ApplicationService.Interfaces
{
    public interface IAuthApplicationService
    {
        AuthResponseDTO AuthUser(AuthDTO auth);
        string CryptPassword(string password, byte[] saltBytes = null);
    }
}
