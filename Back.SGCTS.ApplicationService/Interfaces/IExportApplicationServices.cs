﻿using Back.SGCTS.DTO.Exports;
using System.IO;

namespace Back.SGCTS.ApplicationService.Interfaces
{
    public interface IExportApplicationServices
    {
        byte[] GenerateDataForExport(RequestDataExport requestDataExportBase);

        void ImportData(MemoryStream stream, int userId);
    }
}
