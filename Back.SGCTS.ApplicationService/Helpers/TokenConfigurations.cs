﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Back.SGCTS.ApplicationService.Helpers
{
    public class TokenConfigurations
    {
        public string Audience { get; set; }
        public string Issuer { get; set; }
        public int Seconds { get; set; }
        public string Secret { get; set; }
    }
}
