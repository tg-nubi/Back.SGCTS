﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Back.SGCTS.ApplicationService.Helpers
{
    public class ConfigEmail
    {
        public string Smtp { get; set; }
        public int PortSmtp { get; set; }
        public string Email { get; set; }
        public string User { get; set; }
        public string Password { get; set; }    
        public string Name { get; set; }
    }
}
