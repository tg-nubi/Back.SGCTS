﻿using System;
using System.Collections.Generic;
using System.Text;
using Back.SGCTS.Domain.Domain;
using Back.SGCTS.Domain.Interface;
using Back.SGCTS.Domain.Interface.Services;
using Back.SGCTS.Domain.Interface.Repository;
using Back.SGCTS.Domain.Enum;

namespace Back.SGCTS.ApplicationService.Services
{
    public class CaseAppService : ICaseAppService
    {
        private readonly ICaseRepository _caseRepository;
        public CaseAppService(ICaseRepository caseRepository)
        {
            _caseRepository = caseRepository;
        }

        public IEnumerable<Case> GetAll()
        {
            return _caseRepository.GetAll();
        }

        public Case GetById(int id)
        {
            return _caseRepository.GetById(id);
        }

        public IEnumerable<Case> GetByScenarioId(int id)
        {
            return _caseRepository.GetByScenarioId(id);
        }

        public int Insert(Case cases)
        {
            cases.Id = Convert.ToInt32(_caseRepository.Insert(cases));
            return cases.Id;
        }

        public bool Update(Case cases)
        {
            bool sucess = _caseRepository.Update(cases);
            return sucess;
        }
        public bool UpdateStatus(int id, StatusTypeEnum statusTypeEnum)
        {
            var entity = GetById(id);
            entity.StatusType = statusTypeEnum;
            return Update(entity);
        }
        public IEnumerable<Case> GetByUserId(int id)
        {
            return _caseRepository.GetByUserId(id);
        }
    }
}
