﻿using Back.SGCTS.ApplicationService.Interfaces;
using Back.SGCTS.Domain.Domain;
using Back.SGCTS.Domain.Interface.Repository;
using Back.SGCTS.Domain.Interface.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Back.SGCTS.ApplicationService.Services
{
    public class TreeApplicationService : ITreeApplicationService
    {
        readonly IUserProjectRepository _userProjectRepository;
        readonly ISystemProjectRepository _systemProjectRepository;
        readonly ISystemRepository _systemRepository;
        readonly IProjectAppService _projectAppService;

        public TreeApplicationService(IUserProjectRepository userProjectRepository,
                                      ISystemProjectRepository systemProjectRepository,
                                      ISystemRepository systemRepository,
                                      IProjectAppService projectAppService
                                      )
        {
            _userProjectRepository = userProjectRepository;
            _systemProjectRepository = systemProjectRepository;
            _systemRepository = systemRepository;
            _projectAppService = projectAppService;
        }
        public IList<Domain.Domain.System> Get(int userId)
        {
            var projectsIds = _userProjectRepository.GetAllProjectsByUser(userId);
            var systemsProjects = _systemProjectRepository.GetSistemsByProjects(projectsIds).GroupBy(s => s.SystemId);

            var tree = new List<Domain.Domain.System>();
            foreach(var item in systemsProjects)
            {
                var system = _systemRepository.GetById(item.Key);
                system.Projects = GetProjects(item.Select(i => i.ProjectId));
                tree.Add(system);
            }

            return tree;
        }

        private IEnumerable<Project> GetProjects(IEnumerable<int> projectsIds)
        {
            var projects = new List<Project>();
            foreach (var projectId in projectsIds)
            {
                projects.Add(_projectAppService.GetFullProject(projectId));
            }
            return projects;
        }
    }
}
