﻿using Back.SGCTS.ApplicationService.Helpers;
using Back.SGCTS.ApplicationService.Interfaces;
using Back.SGCTS.ApplicationService.Model;
using Back.SGCTS.Domain.Interface.Repository;
using Back.SGCTS.Domain.ValueObjects;
using Microsoft.Extensions.Options;
using System;
using System.Collections.Generic;
using System.Net;
using System.Net.Mail;
using System.Text;
using Serilog;

namespace Back.SGCTS.ApplicationService.Services
{
    public class EmailService: IEmailService
    {
        private readonly ConfigEmail _emailConfig;
        private readonly IEmailTemplateRepository _emailTemplateRepository;
        
        public EmailService(IEmailTemplateRepository emailTemplateRepository,
                            IOptions<ConfigEmail> emailconfig)
        {
            _emailTemplateRepository = emailTemplateRepository;
            _emailConfig = emailconfig.Value;
        }
        public void SendEmail(Message message)
        {
            var emailTemplate = _emailTemplateRepository.GetById((int)message.Template);
            BuildTemplateMessage(emailTemplate, message);
            SendEmail(emailTemplate, message.Address);
        }
        private void SendEmail(EmailTemplate email, string address)
        {
            MailMessage mail = new MailMessage()
            {
                From = new MailAddress(_emailConfig.Email, _emailConfig.Name),
                To = { new MailAddress(address) },
                Subject = email.Subject,
                Body = email.Body,
                IsBodyHtml = true,
                Priority = MailPriority.Normal,
            };


            using (SmtpClient smtp = new SmtpClient(_emailConfig.Smtp, _emailConfig.PortSmtp) 
            {
                Credentials = new NetworkCredential(_emailConfig.User, _emailConfig.Password),
                EnableSsl = false 
            })
            {
                Log.Information("sendEmail to {@Email}", mail);
                smtp.Send(mail);
                Log.Information("email Sent");
            }
        }
        private void BuildTemplateMessage(EmailTemplate email, Message message)
        {
            email.Subject = ReplaceParameters(email.Subject, message.Parameters);
            email.Body = ReplaceParameters(email.Body, message.Parameters);
        }
        private string ReplaceParameters(string template, IDictionary<string, string> parameters)
        {
            foreach (var param in parameters)
            {
                if (!template.Contains("{{"))
                    break;
                template = template.Replace("{{" + param.Key + "}}", param.Value);
            }
            return template;
        }
    }
}
