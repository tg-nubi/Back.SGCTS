﻿using Back.SGCTS.Domain.Enum;
using Back.SGCTS.Domain.Interface.Repository;
using Back.SGCTS.Domain.Interface.Services;
using Back.SGCTS.Domain.ValueObjects;
using System;
using System.Collections.Generic;
using System.Text;

namespace Back.SGCTS.ApplicationService.Services
{
    public class MenuAppService : IMenuAppService
    {
        private readonly IMenuDapperRepository _menuDapperRepository;

        public MenuAppService(IMenuDapperRepository menuDapperRepository)
        {
            _menuDapperRepository = menuDapperRepository;
        }

        public IEnumerable<Menu> GetMenusByPermission(PermissionTypeEnum permissionType) =>
            _menuDapperRepository.GetMenusByPermission(permissionType);
    }
}
