﻿using System;
using System.Collections.Generic;
using System.Text;
using Back.SGCTS.Domain.Domain;
using Back.SGCTS.Domain.Interface;
using Back.SGCTS.Domain.Interface.Services;
using Back.SGCTS.Domain.Interface.Repository;

namespace Back.SGCTS.ApplicationService.Services
{
    public class StepCaseAppService : IStepCaseAppService
    {
        private readonly IStepCaseRepository _stepCaseRepository;

        public StepCaseAppService(IStepCaseRepository stepCaseRepository)
        {
            _stepCaseRepository = stepCaseRepository;
        }

        public IEnumerable<StepCase> GetAll()
        {
            return _stepCaseRepository.GetAll();
        }

        public IEnumerable<StepCase> GetByCaseId(int caseId)
        {
            return _stepCaseRepository.GetByCaseId(caseId);
        }

        public StepCase GetById(int id)
        {
            return _stepCaseRepository.GetById(id);
        }

        public int Insert(StepCase stepCase)
        {
            stepCase.Id = Convert.ToInt32(_stepCaseRepository.Insert(stepCase));
            return stepCase.Id;
        }

        public bool Update(StepCase stepCase)
        {
            bool sucess = _stepCaseRepository.Update(stepCase);
            return sucess;
        }

        public bool Delete(int stepCaseId)
        {
            bool sucess = _stepCaseRepository.Delete(stepCaseId);
            return sucess;
        }
    }
}
