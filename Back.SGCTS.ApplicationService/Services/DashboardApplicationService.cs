﻿using Back.SGCTS.ApplicationService.Interfaces;
using Back.SGCTS.Domain.Interface.Repository;
using Back.SGCTS.DTO.Dashboard;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Back.SGCTS.ApplicationService.Services
{
    public class DashboardApplicationService : IDashboardApplicationService
    {
        private readonly IUserProjectRepository _userProjectRepository;
        private readonly ISystemProjectRepository _systemProjectRepository;
        private readonly IDashboardRepository _dashboardRepository;
        private readonly ISystemRepository _systemRepository;

        public DashboardApplicationService(IUserProjectRepository userProjectRepository,
                                           ISystemProjectRepository systemProjectRepository,
                                           IDashboardRepository dashboardRepository,
                                           ISystemRepository systemRepository)
        {
            _userProjectRepository = userProjectRepository;
            _systemProjectRepository = systemProjectRepository;
            _dashboardRepository = dashboardRepository;
            _systemRepository = systemRepository;
        }

        public IList<GeneralDashboardDTO> GetGeneralDashboard(int userId)
        {
            var projectsIds = _userProjectRepository.GetAllProjectsByUser(userId);
            var systemsProjects = _systemProjectRepository.GetSistemsByProjects(projectsIds).GroupBy(s => s.SystemId);

            var systemsDash = new List<GeneralDashboardDTO>();
            foreach(var system in systemsProjects)
            {
                var systemDash = new GeneralDashboardDTO
                {
                    Title = _systemRepository.GetById(system.Key).Name,
                    General = _dashboardRepository.GetQuantityOfStatusBySystem(system.Key),
                    Execution =  GetExecutionDashboardDTO(system.Key)

                };
                systemsDash.Add(systemDash);
            }

            return systemsDash;
        }
        private ExecutionDashboardDTO GetExecutionDashboardDTO(int systemId)
        {
            var result = _dashboardRepository.GetExecutionsByStatusFromSystem(systemId).ToList();

            foreach (var item in result.GroupBy(s => s.Status))
            {
                var date = DateTime.Now.AddDays(-7);

                while( date <= DateTime.Now)
                {
                    if (!item.Any(s => s.ExecutionDate.ToString("dd/MM/yyyy") == date.ToString("dd/MM/yyyy")))
                    {
                        result.Add(new Domain.Dashboard.ExecutionByStatus
                        {
                            Count = 0,
                            ExecutionDate = date,
                            Status = item.Key
                        });
                    }
                    date = date.AddDays(1);
                }
                 
            }
            result = result.OrderBy(s => s.ExecutionDate).ToList();

            var dash = new ExecutionDashboardDTO()
            {
                Date = result.Select(s => s.ExecutionDate.ToString("dd/MM/yyyy")).Distinct()
            };

            foreach (var status in result.GroupBy(s => s.Status))
            {
                dash.Series.Add(
                    new ExecutionSeriesDashboardDTO()
                    {
                        Data = status.Select(s => s.Count),
                        Name = status.Key
                    });
            }
            return dash;
        }
       
    }
}
