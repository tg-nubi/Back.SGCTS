﻿using Back.SGCTS.ApplicationService.Extension;
using Back.SGCTS.ApplicationService.Helpers;
using Back.SGCTS.ApplicationService.Interfaces;
using Back.SGCTS.Domain.Domain;
using Back.SGCTS.Domain.Interface;
using Back.SGCTS.DTO.DTO;
using Microsoft.Extensions.Options;
using Microsoft.IdentityModel.Tokens;
using System;
using System.Collections.Generic;
using System.IdentityModel.Tokens.Jwt;
using System.Security.Claims;
using System.Security.Cryptography;
using System.Security.Principal;
using System.Text;

namespace Back.SGCTS.ApplicationService.Services
{
    public class AuthApplicationService : IAuthApplicationService
    {
        private readonly IUserRepository _userRepository;
        private readonly AppSettings _appSettings;
        private readonly int _saltleght = 16 ; 

        public AuthApplicationService(IUserRepository userRepository,
                                      IOptions<AppSettings> appSettings)
        {
            _userRepository = userRepository;
            _appSettings = appSettings.Value;
        }
        public AuthResponseDTO AuthUser(AuthDTO auth)
        {
            var user = _userRepository.GetUser(auth.User);
            if (user != null && 
                user.IsActive() &&
                ValidateLogin(auth.Password, user.Password))
            {
                var authUser = GenerateToken(user);
                authUser.Name = $"{user.Name} {user.LastName}";
                authUser.Permission = user.PermissionType.GetDescription();
                authUser.ResetPasswordRequired = user.ChangePassword;
                authUser.Login = user.Login;
                return authUser;
            }
            return null;
        }
        
        public string CryptPassword(string password, byte[] saltBytes = null)
        {
            var passwordBytes = Encoding.UTF8.GetBytes(password);
            if (saltBytes == null)
                saltBytes = GenerateSalt();

            var passwordWithSaltBytes = new byte[passwordBytes.Length + saltBytes.Length];

            passwordBytes.CopyTo(passwordWithSaltBytes, 0);
            saltBytes.CopyTo(passwordWithSaltBytes, password.Length);

            var passwordCrypt = new SHA1Managed().ComputeHash(passwordWithSaltBytes);
            var passwordCryptAndSault = new byte[passwordCrypt.Length + saltBytes.Length];
            saltBytes.CopyTo(passwordCryptAndSault, 0);
            passwordCrypt.CopyTo(passwordCryptAndSault, _saltleght);

            return Convert.ToBase64String(passwordCryptAndSault);
        }
        private bool ValidateLogin(string password, string passwordCrypt)
        {
            var salt = new byte[_saltleght];
            var passwordByte = Convert.FromBase64String(passwordCrypt);
            Array.Copy(passwordByte, 0, salt, 0, _saltleght);
            return CryptPassword(password, salt) == passwordCrypt;
        }
        private byte[] GenerateSalt()
        {
            var tokenBytes = new byte[_saltleght];
            new RNGCryptoServiceProvider().GetBytes(tokenBytes);
            return tokenBytes;
        }
        private AuthResponseDTO GenerateToken(User user )
        {
            var tokenHandler = new JwtSecurityTokenHandler();
            var key = Encoding.ASCII.GetBytes(_appSettings.TokenConfigurations.Secret);
            var tokenDescriptor = new SecurityTokenDescriptor
            {
                Subject = new ClaimsIdentity(

                    new Claim[] {
                        new Claim(JwtRegisteredClaimNames.Jti, Guid.NewGuid().ToString("N")),
                        new Claim(JwtRegisteredClaimNames.UniqueName, user.Id.ToString()),
                        new Claim(ClaimTypes.Name, user.Id.ToString()),
                        new Claim("userId", user.Id.ToString()),
                        new Claim("userName", $"{user.Name} {user.LastName}"),
                        new Claim(ClaimTypes.Role, user.PermissionTypeId.ToString()),
                        
                    }),
                Expires = DateTime.UtcNow.AddSeconds(_appSettings.TokenConfigurations.Seconds),
                Issuer = _appSettings.TokenConfigurations.Issuer,
                Audience = _appSettings.TokenConfigurations.Audience,
                SigningCredentials = new SigningCredentials(new SymmetricSecurityKey(key), SecurityAlgorithms.HmacSha256Signature),

            };
            var token = tokenHandler.CreateToken(tokenDescriptor);

            return new AuthResponseDTO() { 
                        Token = tokenHandler.WriteToken(token), 
                        Expiration =token.ValidTo.ToLocalTime() };
        }
    }
}
