﻿using Back.SGCTS.Domain.Domain;
using Back.SGCTS.Domain.Enum;
using Back.SGCTS.Domain.Interface.Repository;
using Back.SGCTS.Domain.Interface.Services;
using System;
using System.Collections.Generic;
using System.Text;

namespace Back.SGCTS.ApplicationService.Services
{
    public class TrashAppService : ITrashAppService
    {
        private readonly ITrashRepository _trashRepository;
        private readonly IScenarioAppService _scenarioAppService;
        private readonly ISystemAppService _systemAppService;
        private readonly IProjectAppService _projectAppService;
        private readonly ICaseAppService _caseAppService;

        public TrashAppService(ITrashRepository trashRepository,
                               IScenarioAppService scenarioAppService,
                               ISystemAppService systemAppService,
                               IProjectAppService projectAppService,
                               ICaseAppService caseAppService)
        {
            _trashRepository = trashRepository;
            _scenarioAppService = scenarioAppService;
            _systemAppService = systemAppService;
            _projectAppService = projectAppService;
            _caseAppService = caseAppService;
        }

        public IEnumerable<Trash> GetAll()
        {
            return _trashRepository.GetAll();
        }

        public Trash GetById(int id)
        {
            return _trashRepository.GetById(id);
        }

        public int Insert(Trash entity)
        {
            switch (entity.Type)
            {
                case ObjectTypeEnum.System:
                    entity.Name = _systemAppService.GetById(entity.ObjectId).Name;
                    break;

                case ObjectTypeEnum.Scenario:
                    entity.Name = _scenarioAppService.GetById(entity.ObjectId).Name;
                    break;

                case ObjectTypeEnum.Project:
                    entity.Name = _projectAppService.GetById(entity.ObjectId).Name;
                    break;

                case ObjectTypeEnum.Case:
                    entity.Name = _caseAppService.GetById(entity.ObjectId).Name;
                    break;
            }
            return (int)_trashRepository.Insert(entity);
        }

        public void Recovery(Trash trash)
        {
            var recoveryStatus = StatusTypeEnum.Ativo;
            switch (trash.Type)
            {
                case ObjectTypeEnum.System:
                    _systemAppService.UpdateStatus(trash.ObjectId, recoveryStatus);
                    break;

                case ObjectTypeEnum.Scenario:
                    _scenarioAppService.UpdateStatus(trash.ObjectId, recoveryStatus);
                    break;

                case ObjectTypeEnum.Project:
                    _projectAppService.UpdateStatus(trash.ObjectId, recoveryStatus);
                    break;

                case ObjectTypeEnum.Case:
                    _caseAppService.UpdateStatus(trash.ObjectId, recoveryStatus);
                    break;
            }
            _trashRepository.Delete(trash);
        }

        public bool Update(Trash entity)
        {
            return _trashRepository.Update(entity);
        }
    }
}
