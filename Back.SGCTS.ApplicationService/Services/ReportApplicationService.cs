﻿using Back.SGCTS.ApplicationService.Factories.Reports;
using Back.SGCTS.ApplicationService.Interfaces;
using Back.SGCTS.DTO.Reports;
using iTextSharp.text;
using iTextSharp.text.pdf;
using Microsoft.AspNetCore.Mvc;
using System.IO;

namespace Back.SGCTS.ApplicationService.Services
{
    public class ReportApplicationService : IReportApplicationServices
    {
        private readonly ReportFactory _reportFactory;

        public ReportApplicationService(ReportFactory reportFactory)
        {
            _reportFactory = reportFactory;
        }

        public FileStreamResult GenerateReport(RequestReportBase requestReportBase)
        {
            IReportService reportService = _reportFactory.GetInstance(requestReportBase);

            var system = reportService.GetReport(requestReportBase);

            var report = CreateReport(system);

            return report;
        }
        public FileStreamResult CreateReport(Domain.Domain.System system)
        {
            MemoryStream workStream = new MemoryStream();
            Document doc = new Document(PageSize.A4);
            doc.SetMargins(40, 40, 40, 40);

            PdfWriter writer = PdfWriter.GetInstance(doc, workStream);
            writer.CloseStream = false;

            doc.Open();

            Paragraph title = new Paragraph();
            title.Font = new Font(Font.FontFamily.TIMES_ROMAN, 30);
            title.Alignment = Element.ALIGN_CENTER;
            title.Add("Relatório de Testes \n");
            doc.Add(title);

            string simg = "https://app-front-sgcts.herokuapp.com/img/nubi_dois.adcbb146.png";
            Image img = Image.GetInstance(simg);
            img.ScaleAbsolute(250, 150);
            img.Alignment = Element.ALIGN_CENTER;
            doc.Add(img);

            Paragraph sistema = new Paragraph();
            sistema.Font = new Font(Font.FontFamily.TIMES_ROMAN, 20);
            sistema.Alignment = Element.ALIGN_LEFT;
            sistema.Add("SISTEMA: " + system.Name.Trim() + "\n");
            doc.Add(sistema);

            foreach (var project in system.Projects)
            {
                Paragraph projeto = new Paragraph("", new Font(Font.NORMAL, 20));
                projeto.Add("PROJETO:" + project.Name.Trim() + "\n");
                doc.Add(projeto);

                foreach (var scenario in project.Scenarios)
                {
                    Paragraph cenario = new Paragraph("", new Font(Font.NORMAL, 20));
                    cenario.Add("  CENÁRIO:" + scenario.Name.Trim() + "\n");
                    doc.Add(cenario);

                    Paragraph casoTeste = new Paragraph("", new Font(Font.NORMAL, 15));
                    casoTeste.Add("      CASOS DE TESTE: \n");
                    doc.Add(casoTeste);

                    Paragraph espaco = new Paragraph("", new Font(Font.NORMAL, 15));
                    espaco.Add("\n");
                    doc.Add(espaco);

                    PdfPTable tableHeader = new PdfPTable(4);

                    tableHeader.AddCell("NOME");
                    tableHeader.AddCell("RESULTADO ESPERADO");
                    tableHeader.AddCell("STATUS");
                    tableHeader.AddCell("USUÁRIO");

                    doc.Add(tableHeader);

                    PdfPTable table = new PdfPTable(4);

                    foreach (var caso in scenario.Cases)
                    {
                        table.AddCell(caso.Name);
                        table.AddCell(caso.ResultExpected);
                        table.AddCell(caso.StatusType.ToString());
                        table.AddCell(caso.User.Name + " " + caso.User.LastName);
                    }

                    doc.Add(table);
                }

            }

            doc.Close();

            byte[] byteInfo = workStream.ToArray();
            workStream.Write(byteInfo, 0, byteInfo.Length);
            workStream.Position = 0;

            return new FileStreamResult(workStream, "aplication/pdf");
        }
    }
}
