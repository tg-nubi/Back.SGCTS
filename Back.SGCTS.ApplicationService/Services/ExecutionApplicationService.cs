using AutoMapper;
using Back.SGCTS.Domain.Domain;
using Back.SGCTS.Domain.Interface.Repository;
using Back.SGCTS.Domain.Interface.Services;
using Back.SGCTS.DTO.DTO;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Back.SGCTS.ApplicationService.Services
{
    public class ExecutionApplicationService : IExecutionAppService
    {
        private readonly IExecutionRepository _executionRepository;
        private readonly ICaseAppService _caseAppService;
        private readonly IStepCaseAppService _stepCaseAppService;
        private readonly IScenarioAppService _scenarioAppService;
        private readonly IProjectAppService _projectAppService;
        private readonly ISystemAppService _systemAppService;
        private readonly IMapper _mapper;
        private readonly IUserAppService _userAppService;

        public ExecutionApplicationService(IExecutionRepository executionRepository,
                                           ICaseAppService caseAppService,
                                           IStepCaseAppService stepCaseAppService,
                                           IScenarioAppService scenarioAppService,
                                           IProjectAppService projectAppService,
                                           ISystemAppService systemAppService,
                                           IUserAppService userAppService,
                                           IMapper mapper)
        {
            _executionRepository = executionRepository;
            _caseAppService = caseAppService;
            _stepCaseAppService = stepCaseAppService;
            _scenarioAppService = scenarioAppService;
            _projectAppService = projectAppService;
            _systemAppService = systemAppService;
            _userAppService = userAppService;
            _mapper = mapper;
        }

        public IEnumerable<Execution> GetAll()
        {
            return _executionRepository.GetAll();
        }

        public Execution GetById(int id)
        {
            return _executionRepository.GetById(id);
        }

        public int Insert(Execution execution)
        {
            var caso = _caseAppService.GetById(execution.CaseId);
            caso.StatusType = execution.StatusType;
            _caseAppService.Update(caso);
            return Convert.ToInt32(_executionRepository.Insert(execution));
        }

        public bool Update(Execution entity)
        {
            return _executionRepository.Update(entity);
        }

        public LoadExecutionDTO LoadExecution(int caseId)
        {
            Case caso = _caseAppService.GetById(caseId);

            List<StepCaseDTO> steps = _mapper.Map<List<StepCaseDTO>>(_stepCaseAppService.GetByCaseId(caseId));

            Scenario scenario = _scenarioAppService.GetById(caso.ScenarioId);

            Project project = _projectAppService.GetById(scenario.ProjectId);
            project.SystemsIds = _projectAppService.GetSystemsOnProject(scenario.ProjectId).ToList();

            Domain.Domain.System system = _systemAppService.GetById(project.SystemsIds.FirstOrDefault());

            return new LoadExecutionDTO()
            {
                System = system.Name,
                Project = project.Name,
                Scenario = scenario.Name,
                Case = caso.Name,
                CaseId = caso.Id,
                StepCase = steps
            };
        }

        public IEnumerable<Execution> GetExecutionByCase(int caseId)
        {
            var executions = _executionRepository.GetExecutionByCase(caseId);
            foreach(var execution in executions)
            {
                execution.User = _userAppService.GetById(execution.UserId);
            }
            return executions;
        }

    }
}
