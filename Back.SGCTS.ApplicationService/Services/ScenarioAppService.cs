﻿using System;
using System.Collections.Generic;
using System.Text;
using Back.SGCTS.Domain.Domain;
using Back.SGCTS.Domain.Interface;
using Back.SGCTS.Domain.Interface.Services;
using Back.SGCTS.Domain.Interface.Repository;
using Back.SGCTS.Domain.Enum;

namespace Back.SGCTS.ApplicationService.Services
{
    public class ScenarioAppService : IScenarioAppService
    {
        private readonly IScenarioRepository _scenarioRepository;
        private readonly ICaseAppService _caseAppService;

        public ScenarioAppService(IScenarioRepository scenarioRepository,
                                  ICaseAppService caseAppService)
        {
            _scenarioRepository = scenarioRepository;
            _caseAppService = caseAppService;
        }

        public IEnumerable<Scenario> GetAll()
        {
            return _scenarioRepository.GetAll();
        }

        public Scenario GetById(int id)
        {
            return _scenarioRepository.GetById(id);
        }

        public IEnumerable<Scenario> GetByProjectId(int projectId)
        {
            return _scenarioRepository.GetByProjectId(projectId);
        }

        public IEnumerable<Scenario> GetFullByProjectId(int projectId)
        {
            var scenarios = GetByProjectId(projectId);
            foreach(var scenario in scenarios)
            {
                scenario.Cases = _caseAppService.GetByScenarioId(scenario.Id);
            }
            return scenarios;
        }

        public int Insert(Scenario scenario)
        {
            scenario.Id = Convert.ToInt32(_scenarioRepository.Insert(scenario));
            return scenario.Id;
        }

        public bool Update(Scenario scenario)
        {
            bool sucess = _scenarioRepository.Update(scenario);
            return sucess;
        }
        public bool UpdateStatus(int id, StatusTypeEnum statusTypeEnum)
        {
            var entity = GetById(id);
            entity.StatusType = statusTypeEnum;
            return Update(entity);
        }
    }
}
