﻿using Back.SGCTS.Domain.Domain;
using Back.SGCTS.Domain.Enum;
using Back.SGCTS.Domain.Interface.Repository;
using Back.SGCTS.Domain.Interface.Services;
using Back.SGCTS.Domain.ValueObjects;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Back.SGCTS.ApplicationService.Services
{
    public class ProjectAppService : IProjectAppService
    {
        private readonly IProjectRepository _projectRepository;
        private readonly IUserProjectRepository _userProjectRepository;
        private readonly ISystemProjectRepository _systemProjectRepository;
        private readonly IScenarioAppService _scenarioAppService;
        public ProjectAppService(IProjectRepository projectRepository,
                                 IUserProjectRepository userProjectRepository,
                                 ISystemProjectRepository systemProjectRepository,
                                 IScenarioAppService scenarioAppService)
        {
            _projectRepository = projectRepository;
            _userProjectRepository = userProjectRepository;
            _systemProjectRepository = systemProjectRepository;
            _scenarioAppService = scenarioAppService;
        }

        public IEnumerable<Project> GetAll()
        {
           return _projectRepository.GetAll();
        }

        public Project GetById(int id)
        {
            return _projectRepository.GetById(id);
        }

        public int Insert(Project entity)
        {
            entity.Id = Convert.ToInt32(_projectRepository.Insert(entity));
            SetUsers(entity);
            SetSystems(entity);
            return entity.Id;
        }

        public bool Update(Project entity)
        {
            if (entity.UsersIds != null)
                SetUsers(entity);
            if(entity.SystemsIds != null)
                SetSystems(entity);
            return _projectRepository.Update(entity);
        }

        public void SetUsers(Project project)
        {
            var currentUsers = GetUsersOnProject(project.Id);

            var removeUsers = currentUsers.Where(user => !project.UsersIds.Contains(user));
            foreach(var user in removeUsers)
            {
                _userProjectRepository.Delete(new UserProject() { ProjectId = project.Id, UserId = user });
            }

            var addUsers = project.UsersIds.Where(user => !currentUsers.Contains(user));

            foreach (var user in addUsers)
            {
                _userProjectRepository.Insert(new UserProject() { ProjectId = project.Id, UserId = user });
            }
        }

        public void SetSystems(Project project)
        {
            var currentSystems = GetSystemsOnProject(project.Id);

            var removeSystems = currentSystems.Where(system => !project.SystemsIds.Contains(system));
            foreach (var system in removeSystems)
            {
                _systemProjectRepository.Delete(new SystemProject() { ProjectId = project.Id, SystemId = system });
            }

            var addSystems = project.SystemsIds.Where(system => !currentSystems.Contains(system));

            foreach (var system in addSystems)
            {
                _systemProjectRepository.Insert(new SystemProject() { ProjectId = project.Id, SystemId = system });
            }
        }

        public IEnumerable<int> GetUsersOnProject(int projectId)
        {
            return _userProjectRepository.GetCurrentUsers(projectId);
        }

        public IEnumerable<int> GetSystemsOnProject(int projectId)
        {
            return _systemProjectRepository.GetCurrentSystems(projectId);
        }

        public Project GetFullProject(int projectId)
        {
            var project = GetById(projectId);
            project.Scenarios = _scenarioAppService.GetFullByProjectId(projectId);
            return project;
        }
        public bool UpdateStatus(int id, StatusTypeEnum statusTypeEnum)
        {
            var entity = GetById(id);
            entity.StatusType = statusTypeEnum;
            return Update(entity);
        }
    }
}
