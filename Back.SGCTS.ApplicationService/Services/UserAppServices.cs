﻿using Back.SGCTS.ApplicationService.Exceptions;
using Back.SGCTS.ApplicationService.Interfaces;
using Back.SGCTS.Domain.Domain;
using Back.SGCTS.Domain.Interface;
using Back.SGCTS.Domain.Interface.Services;
using Back.SGCTS.DTO.Base;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;

namespace Back.SGCTS.ApplicationService.Services
{
    public class UserAppServices : IUserAppService
    {
        private readonly IUserRepository _userRepository;
        private readonly IAuthApplicationService _authApplicationService;
        private readonly IEmailService _emailService;
        public UserAppServices(IUserRepository userRepository,
                               IAuthApplicationService authApplicationService,
                               IEmailService emailService)
        {
            _userRepository = userRepository;
            _authApplicationService = authApplicationService;
            _emailService = emailService;
        }

        public IEnumerable<User> GetAll()
        {
            return _userRepository.GetAll();
        }

        public User GetById(int id)
        {
            return _userRepository.GetById(id);
        }

        public int Insert(User user)
        {
            user.ChangePassword = true;
            var tempPassword = Guid.NewGuid().ToString().Substring(0, 8);
            user.Login = GetNewLogin(user.Name, user.LastName);
            user.Password = _authApplicationService.CryptPassword(tempPassword);

            SendWelcomeEmail(user.Email, user.Name, user.Login, tempPassword);

            user.Id = Convert.ToInt32(_userRepository.Insert(user));
            return user.Id;
        }

        public BaseResponseEntityDTO<string> ResetPassword(string login)
        {
            var user = _userRepository.GetUser(login);
            if (user == null || !user.IsActive())
                throw new NotFoundException("Usuario não localizado ou inativo");
            
            var tempPassword = Guid.NewGuid().ToString().Substring(0, 8);
            user.Password = _authApplicationService.CryptPassword(tempPassword);
            user.ChangePassword = true;
            _userRepository.Update(user);
            SendResetPasswordEmail(user.Email, user.Name, user.Login, tempPassword);
            return new BaseResponseEntityDTO<string>(user.GetHideEmail());
        }

        public bool Update(User user)
        {
            var oldUser = GetById(user.Id);
            user.Password = oldUser.Password;
            user.ChangePassword = oldUser.ChangePassword;
            bool sucess = _userRepository.Update(user);
            return sucess;
        }
        public void CreatePassword(string login, string password)
        {
            var user = _userRepository.GetUser(login);
            user.Password = _authApplicationService.CryptPassword(password);
            user.ChangePassword = false;
            _userRepository.Update(user);
        }
        private string GetNewLogin(string firstName, string lastName)
        {
            var login = $"{firstName.ToLower().Trim().Split(" ").First()}.{lastName.ToLower().Trim().Split(" ").Last()}";
            var logins = _userRepository.GetExistsLogins(login);
            if (logins == null || logins.Count() ==0 )
                return login;
            if (logins.Count() == 1)
                return login + 1;
            int lastSequenceLogin = Convert.ToInt32(Regex.Match(logins.Last(), @"\d+").Value);
            return string.Concat(login, lastSequenceLogin + 1);
        }

        private void SendWelcomeEmail(string email,
                                      string name,
                                      string login,
                                      string password)
        {
            var parameters = new Dictionary<string, string>() {
                            { "Name", name },
                            { "Login", login },
                            { "Password", password} };
            _emailService.SendEmail(new Model.Message()
            {
                Address = email,
                Template = Domain.Enum.EmailTypeEnum.Welcome,
                Parameters = parameters
            });
        }
        private void SendResetPasswordEmail(string email,
                                      string name,
                                      string login,
                                      string password)
        {
            var parameters = new Dictionary<string, string>() {
                            { "Name", name },
                            { "Login", login },
                            { "Password", password} };
            _emailService.SendEmail(new Model.Message()
            {
                Address = email,
                Template = Domain.Enum.EmailTypeEnum.ResetPassword,
                Parameters = parameters
            });
        }
    }
}
