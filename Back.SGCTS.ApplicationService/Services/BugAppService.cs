﻿using Back.SGCTS.Domain.Domain;
using Back.SGCTS.Domain.Interface.Repository;
using Back.SGCTS.Domain.Interface.Services;
using System;
using System.Collections.Generic;
using System.Text;

namespace Back.SGCTS.ApplicationService.Services
{
    public class BugAppService : IBugAppService
    {
        private readonly IBugRepository _bugRepository;
        public BugAppService(IBugRepository bugRepository)
        {
            _bugRepository = bugRepository;
        }
        public int Insert(Bug bug)
        {
            return Convert.ToInt32(_bugRepository.Insert(bug));
        }

        public bool Update(Bug bug)
        {
            return _bugRepository.Update(bug);
        }

        public Bug GetById(int bugId)
        {
            return _bugRepository.GetById(bugId);
        }

        public IEnumerable<Bug> GetAll()
        {
            return _bugRepository.GetAll();
        }

        public IEnumerable<Bug> GetByUserId(int id)
        {
            return _bugRepository.GetByUserId(id);
        }
    }
}
