﻿using AutoMapper;
using Back.SGCTS.ApplicationService.Interfaces;
using Back.SGCTS.Domain.Domain;
using Back.SGCTS.Domain.Enum;
using Back.SGCTS.Domain.Interface.Repository;
using Back.SGCTS.Domain.Interface.Services;
using Back.SGCTS.DTO.DTO;
using Back.SGCTS.DTO.Exports;
using Microsoft.AspNetCore.Mvc;
using OfficeOpenXml;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;

namespace Back.SGCTS.ApplicationService.Services
{
    public class ExportApplicationService : IExportApplicationServices
    {
        private readonly IExportRepository _exportRepository;
        private readonly IStepCaseRepository _stepCaseRepository;
        private readonly ICaseRepository _caseRepository;
        private readonly IProjectRepository _projectRepository;
        private readonly IScenarioRepository _scenarioRepository;

        public ExportApplicationService(IStepCaseRepository stepCaseRepository,
                                 IExportRepository exportRepository,
                                 ICaseRepository caseRepository,
                                 IProjectRepository projectRepository,
                                 IScenarioRepository scenarioRepository)
        {
            _scenarioRepository = scenarioRepository;
            _projectRepository = projectRepository;
            _caseRepository = caseRepository;
            _stepCaseRepository = stepCaseRepository;
            _exportRepository = exportRepository;
        }

        public byte[] GenerateDataForExport(RequestDataExport requestDataExport)
        {
            IEnumerable<Export> excel = null;
            if (requestDataExport.SystemId.HasValue)
            {
                if (requestDataExport.ProjectId.HasValue)
                {
                    if (requestDataExport.ScenarioId.HasValue)
                    {
                        excel = _exportRepository.GetExportWithAllParameters( requestDataExport.SystemId.Value, requestDataExport.ProjectId.Value, requestDataExport.ScenarioId.Value);

                    }
                    excel = _exportRepository.GetExportWithProject(requestDataExport.SystemId.Value, requestDataExport.ProjectId.Value);
                }

                excel = _exportRepository.GetExportWithSystem(requestDataExport.SystemId.Value);
            }
            return creatDocument(excel);
        }

        public byte[] creatDocument(IEnumerable<Export> data)
        {
            ExcelPackage.LicenseContext = LicenseContext.NonCommercial;
            var file = new FileInfo(@"exportData.xlsx");

            var package = new ExcelPackage(file);

            var ws = package.Workbook.Worksheets.Add("Export");
            var range = ws.Cells["A1"].LoadFromCollection(data, true);

            range.AutoFitColumns();

            ws.Row(1).Style.Font.Bold = true;
            ws.Row(1).Style.HorizontalAlignment = OfficeOpenXml.Style.ExcelHorizontalAlignment.Center;
            ws.Column(9).Style.HorizontalAlignment = OfficeOpenXml.Style.ExcelHorizontalAlignment.Center;

            var bytes = package.GetAsByteArray();

            return bytes;

        }

        public void ImportData(MemoryStream stream, int userId)
        {
            ExcelPackage.LicenseContext = LicenseContext.NonCommercial;
            var package = new ExcelPackage(stream);

            ExcelWorksheet worksheet = package.Workbook.Worksheets.FirstOrDefault();
            var colCount = worksheet.Dimension.End.Column;
            var rowCount = worksheet.Dimension.End.Row;

            string infoCell;
            string nameProject = "";
            string nameSystem = "";
            var newCase = 0;

            for (int row = 2; row < rowCount; row++)
            {
                var scenario = new Scenario();
                var stepCase = new StepCase();
                var cases = new Case();
                cases.UserId = userId;
                for (int col = 1; col <= colCount; col++)
                {
                    infoCell = worksheet.Cells[row, col].Value?.ToString().Trim();
                    switch (worksheet.Cells[1, col].Value?.ToString().Trim())
                    {
                        case "nameSystem":
                            nameSystem = infoCell;
                            break;
                        case "nameProject":
                            nameProject = infoCell;
                            break;
                        case "nameScenario":
                            scenario.Name = infoCell;
                            break;
                        case "descriptionScenario":
                            scenario.Description = infoCell;
                            break;
                        case "nameCase":
                            cases.Name = infoCell;
                            break;
                        case "preconditionCase":
                            cases.PreCondition = infoCell;
                            break;
                        case "datamassCase":
                            cases.DataMass = infoCell;
                            break;
                        case "resultExpectedCase":
                            cases.ResultExpected = infoCell;
                            break;
                        case "numberStep":
                            stepCase.NumberStep = Int32.Parse(infoCell);
                            break;
                        case "actionStep":
                            stepCase.Action = infoCell;
                            break;
                        case "resultExpectedStep":
                            stepCase.ResultExpected = infoCell;
                            break;
                    }
                }
                var scenarioId = _exportRepository.VerifyExistentScenario(nameSystem, nameProject, scenario.Name).DefaultIfEmpty(0).FirstOrDefault();
                if (scenarioId == 0)
                {
                    scenario.ProjectId = _projectRepository.GetProjectIdByName(nameProject).FirstOrDefault();
                    scenario.StatusTypeId = 1;
                    scenarioId = (int)_scenarioRepository.Insert(scenario);
                }

                var caseId = _exportRepository.VerifyExistentCase(nameSystem, nameProject, scenario.Name, cases.Name).DefaultIfEmpty(0).FirstOrDefault();
                if(caseId == 0)
                {
                    cases.ScenarioId = scenarioId;
                    cases.StatusTypeId = 1;
                    caseId = (int)_caseRepository.Insert(cases);
                    newCase = caseId;
                }
                if(newCase == caseId)
                {
                    stepCase.CaseId = caseId;
                    _stepCaseRepository.Insert(stepCase);
                }
            }
        }
    }
}
