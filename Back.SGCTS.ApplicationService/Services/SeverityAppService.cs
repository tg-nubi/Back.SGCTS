﻿using Back.SGCTS.Domain.Interface.Repository;
using Back.SGCTS.Domain.Interface.Services;
using Back.SGCTS.Domain.ValueObjects;
using System;
using System.Collections.Generic;
using System.Text;

namespace Back.SGCTS.ApplicationService.Services
{
    public class SeverityAppService : ISeverityAppService
    {
        private readonly ISeverityDapperRepository _severityAppRepository;

        public SeverityAppService(ISeverityDapperRepository severityAppRepository)
        {
            _severityAppRepository = severityAppRepository;
        }

        public int Insert(SeverityType severity)
        {
            severity.Id = Convert.ToInt32(_severityAppRepository.Insert(severity));
            return severity.Id;
        }

        public bool Update(SeverityType severity)
        {
            return _severityAppRepository.Update(severity);
        }

        public SeverityType GetById(int severityId)
        {
            return _severityAppRepository.GetById(severityId);
        }

        public IEnumerable<SeverityType> GetAll()
        {
            return _severityAppRepository.GetAll();
        }

    }
}
