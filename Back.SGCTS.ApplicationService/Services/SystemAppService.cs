﻿using Back.SGCTS.Domain.Domain;
using Back.SGCTS.Domain.Interface;
using Back.SGCTS.Domain.Interface.Services;
using Back.SGCTS.Domain.Interface.Repository;
using System;
using System.Collections.Generic;
using System.Text;
using Back.SGCTS.Domain.Enum;

namespace Back.SGCTS.ApplicationService.Services
{
    public class SystemAppService : ISystemAppService
    {
        private readonly ISystemRepository _systemRepository;

        public SystemAppService(ISystemRepository systemRepository)
        {
            _systemRepository = systemRepository; 
        }

        public IEnumerable<Domain.Domain.System> GetAll()
        {
            return _systemRepository.GetAll();
        }

        public Domain.Domain.System GetById(int id)
        {
            return _systemRepository.GetById(id);
        }

        public int Insert(Domain.Domain.System system)
        {
            system.Id = Convert.ToInt32(_systemRepository.Insert(system));
            return system.Id;
        }

        public bool Update(Domain.Domain.System system)
        {
            bool sucess = _systemRepository.Update(system);
            return sucess;
        }
        public bool UpdateStatus(int id, StatusTypeEnum statusTypeEnum)
        {
            var entity = GetById(id);
            entity.StatusType = statusTypeEnum;
            return Update(entity);
        }
    }
}
