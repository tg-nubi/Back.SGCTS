﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Back.SGCTS.ApplicationService.Exceptions
{
    public class NotFoundException : Exception
    {
        public NotFoundException(string message):base(message)
        {
        }
    }
}
