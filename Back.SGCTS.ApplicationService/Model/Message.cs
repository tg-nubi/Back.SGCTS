﻿using Back.SGCTS.Domain.Enum;
using System;
using System.Collections.Generic;
using System.Text;

namespace Back.SGCTS.ApplicationService.Model
{
    public class Message
    {
        public string Address { get; set; }
        public EmailTypeEnum Template { get; set; }
        public IDictionary<string, string> Parameters { get; set; }
    }
}
