﻿using Back.SGCTS.Domain.Interface.Repository;
using Back.SGCTS.Domain.ValueObjects;
using Back.SGCTS.Repository.Common;
using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;
using System.Text;

namespace Back.SGCTS.Repository.Repository
{
    public class SeverityDapperRepository : BaseRepository<SeverityType, int>, ISeverityDapperRepository
    {
        public SeverityDapperRepository(IConfiguration configuration) : base(configuration)
        {
            
        }
    }
}
