﻿using System;
using System.Collections.Generic;
using System.Text;
using Back.SGCTS.Domain.Domain;
using Back.SGCTS.Domain.Enum;
using Back.SGCTS.Domain.Interface.Repository;
using Back.SGCTS.Repository.Common;
using Back.SGCTS.Repository.Scripts;
using Dapper;
using Dommel;
using Microsoft.Extensions.Configuration;

namespace Back.SGCTS.Repository.Repository
{
    public class ScenarioDapperRepository : BaseRepository<Scenario, int>, IScenarioRepository
    {
        public ScenarioDapperRepository(IConfiguration configuration) : base(configuration)
        {
        }
        public override IEnumerable<Scenario> GetAll()
        {
            return _SqlConnection.Select<Scenario>(s => s.StatusTypeId != (int)StatusTypeEnum.Deletado);
        }

        public IEnumerable<Scenario> GetByProjectId(int projectId)
        {
            return _SqlConnection.Query<Scenario>(ScenarioScripts.GetByProjectId(),
                                                new { projectId },
                                                commandType: System.Data.CommandType.Text);
        }
    }
}
