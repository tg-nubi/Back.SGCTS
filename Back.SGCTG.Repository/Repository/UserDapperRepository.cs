﻿using Back.SGCTS.Domain.Domain;
using Back.SGCTS.Domain.Interface;
using Back.SGCTS.Repository.Common;
using Back.SGCTS.Repository.Scripts;
using Dapper;
using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Back.SGCTS.Repository.Repository
{
    public class UserDapperRepository : BaseRepository<User, int> , IUserRepository
    {
        public UserDapperRepository(IConfiguration configuration) : base(configuration)
        {
  
        }

        public IEnumerable<string> GetExistsLogins(string login)
        {
            return _SqlConnection.Query<string>(UserScripts.GetExistsLogins(),
                                                  new { login = $"{ login}%" },
                                                  commandType: System.Data.CommandType.Text).ToList();
        }

        public User GetUser(string user)
        {
            return _SqlConnection.Query<User>(UserScripts.GetUserScript(), 
                                                        new { user }, 
                                                        commandType: System.Data.CommandType.Text).FirstOrDefault();
        }
    }
}
