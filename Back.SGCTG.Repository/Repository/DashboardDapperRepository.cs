﻿using Back.SGCTS.Domain.Dashboard;
using Back.SGCTS.Domain.Interface.Repository;

using Back.SGCTS.Repository.Common;
using Back.SGCTS.Repository.Scripts;
using Dapper;
using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Back.SGCTS.Repository.Repository
{
    public class DashboardDapperRepository : BaseRepository, IDashboardRepository
    {
        public DashboardDapperRepository(IConfiguration configuration) : base(configuration)
        {
        }

        public IDictionary<string, int> GetQuantityOfStatusBySystem(int systemId)
        {
            return _SqlConnection.Query(DashboardScripts.GetQuantityOfStatusBySystem(),
                                                       new { systemId },
                                                       commandType: System.Data.CommandType.Text).ToDictionary(
                                                                row => (string) row.status_type,
                                                                row => (int) row.count);
        }
        public IEnumerable<ExecutionByStatus> GetExecutionsByStatusFromSystem(int systemId)
        {
            return _SqlConnection.Query<ExecutionByStatus>(DashboardScripts.GetExecutionsByStatusFromSystem(),
                                                       new { systemId },
                                                       commandType: System.Data.CommandType.Text);
        }
    }
}
