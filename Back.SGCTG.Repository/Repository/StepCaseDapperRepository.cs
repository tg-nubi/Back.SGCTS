﻿using System;
using System.Collections.Generic;
using System.Text;
using Back.SGCTS.Domain.Domain;
using Back.SGCTS.Domain.Interface.Repository;
using Back.SGCTS.Repository.Common;
using Back.SGCTS.Repository.Scripts;
using Dapper;
using Microsoft.Extensions.Configuration;

namespace Back.SGCTS.Repository.Repository
{
    public class StepCaseDapperRepository : BaseRepository<StepCase, int>, IStepCaseRepository
    {
        public StepCaseDapperRepository(IConfiguration configuration) : base(configuration)
        {
        }

        public IEnumerable<StepCase> GetByCaseId(int caseId)
        {
            return _SqlConnection.Query<StepCase>(StepCaseScripts.GetByCaseId(),
                                            new { caseId },
                                            commandType: System.Data.CommandType.Text);
        }

        public bool Delete(int stepCaseId)
        {
            return _SqlConnection.Execute(StepCaseScripts.Delete(),
                                            new { stepCaseId },
                                            commandType: System.Data.CommandType.Text) > 0;
        }
    }
}
