﻿using Back.SGCTS.Domain.Domain;
using Back.SGCTS.Domain.Enum;
using Back.SGCTS.Domain.Interface.Repository;
using Back.SGCTS.Repository.Common;
using Back.SGCTS.Repository.Scripts;
using Dapper;
using Dommel;
using Microsoft.Extensions.Configuration;
using System.Collections.Generic;

namespace Back.SGCTS.Repository.Repository
{
    public class ProjectDapperRepository : BaseRepository<Project, int>, IProjectRepository
    {
        public ProjectDapperRepository(IConfiguration configuration) : base(configuration)
        {
        }

        public IEnumerable<Project> GetProjectsBySystemId(int systemId)
        {
            return _SqlConnection.Query<Project>(ProjectScripts.GetProjectsBySystemId(),
                                                    new { systemId },
                                                    commandType: System.Data.CommandType.Text);
        }
        public override IEnumerable<Project> GetAll()
        {
            return _SqlConnection.Select<Project>(s => s.StatusTypeId != (int)StatusTypeEnum.Deletado);
        }

        public IEnumerable<int> GetProjectIdByName(string nameProject)
        {
            return _SqlConnection.Query<int>(ProjectScripts.GetProjectIdByName(),
                                                new { nameProject },
                                                commandType: System.Data.CommandType.Text);
        }
    }
}
