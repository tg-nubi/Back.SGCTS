﻿using Back.SGCTS.Domain.Domain;
using Back.SGCTS.Domain.Interface.Repository;
using Back.SGCTS.Domain.ValueObjects;
using Back.SGCTS.Repository.Common;
using Back.SGCTS.Repository.Scripts;
using Dapper;
using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;

namespace Back.SGCTS.Repository.Repository
{
    public class SystemProjectDapperRepository : BaseRepository<SystemProject, int>, ISystemProjectRepository
    {
        public SystemProjectDapperRepository(IConfiguration configuration) : base(configuration)
        {
        }

        public IEnumerable<int> GetCurrentSystems(int projectId)
        {
            return _SqlConnection.Query<int>(SystemProjectScripts.GetCurrentSystems(),
                                                        new { projectId },
                                                        commandType: System.Data.CommandType.Text);
        }
        public int Delete(SystemProject systemProject)
        {
            return _SqlConnection.Execute(SystemProjectScripts.Delete(),
                                             systemProject,
                                             commandType: System.Data.CommandType.Text);
        }
        public IEnumerable<SystemProject> GetSistemsByProjects(IEnumerable<int> projectIds)
        {
            return _SqlConnection.Query<SystemProject>(SystemProjectScripts.GetSistemsByProjects(),
                                                        new { projectIds },
                                                        commandType: System.Data.CommandType.Text);
        }
    }
}
