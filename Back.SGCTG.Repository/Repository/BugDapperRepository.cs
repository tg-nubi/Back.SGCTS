﻿using Back.SGCTS.Domain.Domain;
using Back.SGCTS.Domain.Interface.Repository;
using Back.SGCTS.Repository.Common;
using Back.SGCTS.Repository.Scripts;
using Dapper;
using Microsoft.Extensions.Configuration;
using System.Collections.Generic;

namespace Back.SGCTS.Repository.Repository
{
    public class BugDapperRepository : BaseRepository<Bug, int>, IBugRepository
    {
        public BugDapperRepository(IConfiguration configuration) : base(configuration)
        {
        }

        public IEnumerable<Bug> GetByUserId(int userId)
        {
            return _SqlConnection.Query<Bug>(BugScripts.GetByUserId(),
                                            new { userId },
                                            commandType: System.Data.CommandType.Text);
        }
    }
}
