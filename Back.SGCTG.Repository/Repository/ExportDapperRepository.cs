﻿using Back.SGCTS.Domain.Domain;
using Back.SGCTS.Domain.Interface.Repository;
using Back.SGCTS.DTO.DTO;
using Back.SGCTS.Repository.Common;
using Back.SGCTS.Repository.Scripts;
using Microsoft.Extensions.Configuration;
using Dapper;
using System.Collections.Generic;

namespace Back.SGCTS.Repository.Repository
{
    public class ExportDapperRepository : BaseRepository<StepCase, int>, IExportRepository
    {
        public ExportDapperRepository(IConfiguration configuration) : base(configuration)
        {
        }

        public IEnumerable<Export> GetExportWithAllParameters(int systemId, int projectId, int scenarioId)
        {
            return _SqlConnection.Query<Export>(ExportScripts.GetExportWithAllParameters(),
                                                    new
                                                    {
                                                        systemId, projectId, scenarioId
                                                    },
                                                    commandType: System.Data.CommandType.Text).AsList();
        }

        public IEnumerable<Export> GetExportWithProject(int systemId, int projectId)
        {
            return _SqlConnection.Query<Export>(ExportScripts.GetExportWithProject(),
                                                    new
                                                    {
                                                        systemId,
                                                        projectId
                                                    },
                                                    commandType: System.Data.CommandType.Text).AsList();
        }

        public IEnumerable<Export> GetExportWithSystem(int systemId)
        {
            return _SqlConnection.Query<Export>(ExportScripts.GetExportWithSystem(),
                                                     new
                                                     {
                                                         systemId
                                                     },
                                                     commandType: System.Data.CommandType.Text).AsList();
        }

        public IEnumerable<int> VerifyExistentScenario(string nameSystem, string nameProject, string nameScenario)
        {
            return _SqlConnection.Query<int>(ExportScripts.VerifyExistentScenario(),
                                                new { nameSystem, nameProject, nameScenario },
                                                commandType: System.Data.CommandType.Text);
        }

        public IEnumerable<int> VerifyExistentCase(string nameSystem, string nameProject, string nameScenario, string nameCase)
        {
            return _SqlConnection.Query<int>(ExportScripts.VerifyExistentCase(),
                                                new { nameSystem, nameProject, nameScenario, nameCase },
                                                commandType: System.Data.CommandType.Text);
        }
    }
}
