﻿using Back.SGCTS.Domain.Domain;
using Back.SGCTS.Domain.Interface.Repository;
using Back.SGCTS.DTO.DTO;
using Back.SGCTS.Repository.Common;
using Dommel;
using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;
using System.Text;

namespace Back.SGCTS.Repository.Repository
{
    public class ExecutionDapperRepository : BaseRepository<Execution, int>, IExecutionRepository
    {
        public ExecutionDapperRepository(IConfiguration configuration) : base(configuration)
        {

        }

        IEnumerable<Execution> IExecutionRepository.GetExecutionByCase(int caseId)
        {
            return _SqlConnection.Select<Execution>(s => s.CaseId == caseId);
        }
    }
}
