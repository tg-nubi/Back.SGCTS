﻿using Back.SGCTS.Domain.Enum;
using Back.SGCTS.Domain.Interface.Repository;
using Back.SGCTS.Domain.ValueObjects;
using Back.SGCTS.Repository.Common;
using Back.SGCTS.Repository.Scripts;
using Dapper;
using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;
using System.Text;

namespace Back.SGCTS.Repository.Repository
{
    public class MenuDapperRepository : BaseRepository, IMenuDapperRepository
    {
        public MenuDapperRepository(IConfiguration configuration) : base(configuration)
        {
        }

        public IEnumerable<Menu> GetMenusByPermission(PermissionTypeEnum permissionType)
        {
            return _SqlConnection.Query<Menu>(MenuScripts.GetMenusByPermission(),
                                              new { permissionType = (int)permissionType },
                                              commandType: System.Data.CommandType.Text);
        }
    }
}
