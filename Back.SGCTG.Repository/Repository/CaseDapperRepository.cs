﻿using System;
using System.Collections.Generic;
using Back.SGCTS.Domain.Domain;
using Back.SGCTS.Domain.Interface.Repository;
using Back.SGCTS.Repository.Common;
using Microsoft.Extensions.Configuration;
using Back.SGCTS.Repository.Scripts;
using Dapper;
using Dommel;
using Back.SGCTS.Domain.Enum;

namespace Back.SGCTS.Repository.Repository
{
    public class CaseDapperRepository : BaseRepository<Case, int>, ICaseRepository
    {
        public CaseDapperRepository(IConfiguration configuration) : base(configuration)
        {
        }
        public override IEnumerable<Case> GetAll()
        {
            return _SqlConnection.Select<Case>(s => s.StatusTypeId != (int)StatusTypeEnum.Deletado);
        }

        public IEnumerable<Case> GetByScenarioId(int scenarioId)
        {
            return _SqlConnection.Query<Case>(CaseScripts.GetByScenarioId(),
                                              new { scenarioId },
                                              commandType: System.Data.CommandType.Text);
        }
        public IEnumerable<Case> GetByUserId(int userId)
        {
            return _SqlConnection.Query<Case>(CaseScripts.GetByUserId(),
                                              new { userId },
                                              commandType: System.Data.CommandType.Text);
        }
    }
}
