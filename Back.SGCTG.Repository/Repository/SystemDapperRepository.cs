﻿using Back.SGCTS.Domain.Domain;
using Back.SGCTS.Domain.Enum;
using Back.SGCTS.Domain.Interface.Repository;
using Back.SGCTS.Repository.Common;
using Dommel;
using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;
using System.Text;

namespace Back.SGCTS.Repository.Repository
{
    public class SystemDapperRepository : BaseRepository<Domain.Domain.System, int>, ISystemRepository
    {
        public SystemDapperRepository(IConfiguration configuration) : base(configuration)
        {

        }
        public override IEnumerable<Domain.Domain.System> GetAll()
        {
            return _SqlConnection.Select<Domain.Domain.System>(s => s.StatusTypeId != (int)StatusTypeEnum.Deletado);
        }
    }
}
