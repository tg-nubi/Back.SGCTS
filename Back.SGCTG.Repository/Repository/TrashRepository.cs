﻿using Back.SGCTS.Domain.Domain;
using Back.SGCTS.Domain.Interface.Repository;
using Back.SGCTS.Repository.Common;
using Dommel;
using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;
using System.Text;

namespace Back.SGCTS.Repository.Repository
{
    public class TrashRepository : BaseRepository<Trash, int>, ITrashRepository
    {
        public TrashRepository(IConfiguration configuration) : base(configuration)
        {
        }

        public bool Delete(Trash trash)
        {
            return _SqlConnection.Delete(trash);
        }
    }
}
