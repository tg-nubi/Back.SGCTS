﻿using Back.SGCTS.Domain.Interface.Repository;
using Back.SGCTS.Domain.ValueObjects;
using Back.SGCTS.Repository.Common;
using Back.SGCTS.Repository.Scripts;
using Dapper;
using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;
using System.Text;

namespace Back.SGCTS.Repository.Repository
{
    public class UserProjectDapperRepository : BaseRepository<UserProject, int>, IUserProjectRepository
    {
        public UserProjectDapperRepository(IConfiguration configuration) : base(configuration)
        {
        }

        public IEnumerable<int> GetCurrentUsers(int projectId)
        {
            return _SqlConnection.Query<int>(UserProjectScripts.GetCurrentUsers(),
                                                        new { projectId },
                                                        commandType: System.Data.CommandType.Text);
        }
        public int Delete(UserProject userProject)
        {
            return _SqlConnection.Execute(UserProjectScripts.Delete(),
                                             userProject,
                                             commandType: System.Data.CommandType.Text);
        }
        public IEnumerable<int> GetAllProjectsByUser(int userId)
        {
            return _SqlConnection.Query<int>(UserProjectScripts.GetAllProjectsByUser(),
                                                        new
                                                        {
                                                            userId
                                                        },
                                                        commandType: System.Data.CommandType.Text);

        }
    }
}
