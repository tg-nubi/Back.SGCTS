﻿using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.Text;
using Dommel;
using Microsoft.Extensions.Configuration;
using Back.SGCTS.Domain.Interface;
using Npgsql;

namespace Back.SGCTS.Repository.Common
{
    public abstract class BaseRepository : IDisposable
    {
        protected NpgsqlConnection _SqlConnection;

        public BaseRepository(IConfiguration configuration)
        {
            var connectionString = configuration.GetConnectionString("DB_SGCTS");
            _SqlConnection = new NpgsqlConnection(connectionString);
        }
        public void Dispose()
        {
            _SqlConnection.Dispose();
            _SqlConnection.Close();
        }
    }
    public abstract class BaseRepository<TEntity,TKey> : BaseRepository , IBaseRepository<TEntity,TKey>
        where TEntity : class
    {

        public BaseRepository(IConfiguration configuration) : base(configuration)
        { }
 
        public TEntity GetById(TKey id)
        {
            return _SqlConnection.Get<TEntity>(id);
        }
        public virtual IEnumerable<TEntity> GetAll()
        {
            return _SqlConnection.GetAll<TEntity>();
        }
        public object Insert(TEntity entity)
        {
            return _SqlConnection.Insert(entity);
        }
        public bool Update (TEntity entity)
        {
            return _SqlConnection.Update(entity);
        }

    }
}
