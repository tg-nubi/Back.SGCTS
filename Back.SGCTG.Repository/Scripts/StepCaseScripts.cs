﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Back.SGCTS.Repository.Scripts
{
    public static class StepCaseScripts
    {
        internal static string GetByCaseId()
        {
            return @"SELECT * FROM
                     STEP_CT c 
                     WHERE FK_CASES_STEP_ID = @caseId";
        }

        internal static string Delete()
        {
            return @"DELETE FROM STEP_CT
                     WHERE PK_STEP_CT_ID = @stepCaseId";
        }
    }
}
