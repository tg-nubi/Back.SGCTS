﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Back.SGCTS.Repository.Scripts
{
    public static class CaseScripts
    {
        internal static string GetByScenarioId()
        {
            return @"SELECT * FROM
                     CASES c 
                     WHERE FK_SCENARIO_ID = @scenarioId and fk_status_type_cases_id <> 8 ";
        }

        internal static string GetByUserId()
        {
            return @"SELECT * FROM
                     CASES c
                     WHERE FK_USERS_CASES_ID = @userId
                     and fk_status_type_cases_id not in (8,6)";
        }
    }
}
