﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Back.SGCTS.Repository.Scripts
{
    public static class UserProjectScripts
    {
        internal static string GetCurrentUsers()
        {
            return @"SELECT FK_USERS_ID FROM USERS_PROJECT up
                     WHERE FK_PROJECT_USERS_PROJECT_ID = @ProjectId";
        }

        internal static string Delete()
        {
            return @"DELETE  FROM USERS_PROJECT 
                     WHERE FK_PROJECT_USERS_PROJECT_ID = @ProjectId
                     AND FK_USERS_ID = @UserId";
        }
        internal static string GetAllProjectsByUser()
        {
            return @"SELECT FK_PROJECT_USERS_PROJECT_ID  FROM USERS_PROJECT up
                     inner join project p on p.pk_project_id = up.fk_project_users_project_id
                     WHERE FK_USERS_ID = @UserId and p.fk_status_type_project_id <> 8 ";
        }
    }
}
