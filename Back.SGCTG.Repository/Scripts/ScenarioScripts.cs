﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Back.SGCTS.Repository.Scripts
{
    public static class ScenarioScripts
    {
        internal static string GetByProjectId()
        {
            return @"SELECT * FROM
                     SCENARIO s 
                     WHERE FK_PROJECT_SCENARIO_ID = @projectId and fk_status_type_scenario_id <> 8";
        }
    }
}
