﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Back.SGCTS.Repository.Scripts
{
    public static class UserScripts
    {
        public static string GetUserScript()
        {
            return @"select * from USERS u
                    where LOGIN_USERS = @user";
        }

        internal static string GetExistsLogins()
        {
            return @"SELECT LOGIN_USERS FROM USERS u
                   WHERE LOGIN_USERS LIKE @login
                   ORDER BY LOGIN_USERS";
        }
    }
}
