﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Back.SGCTS.Repository.Scripts
{
    public static class BugScripts
    {
        internal static string GetByUserId()
        {
            return @"SELECT PK_BUG_ID, DESCRIPTION_BUG, TITLE_BUG, 
                            FK_SEVERITY_TYPE_BUG_ID, FK_STATUS_TYPE_BUG_ID, 
                            ACTION_STEP_BUG, FK_PRIORITY_TYPE_BUG_ID, 
                            FK_USER_BUG_ID, FK_EXECUTION_BUG_ID 
                     FROM BUG b
                     WHERE FK_USER_BUG_ID = @userId";
        }
    }
}
