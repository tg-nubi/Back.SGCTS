﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Back.SGCTS.Repository.Scripts
{
    public static class MenuScripts
    {
        public static string GetMenusByPermission()
        {
            return @"select m.* 
                    from menu m 
                    inner join menu_permission mp  on m.pk_menu_id  = mp.fk_menu_id 
                    where mp.fk_permission_id = @permissionType order by 1";
        }
    }
}
