﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Back.SGCTS.Repository.Scripts
{
    public static class ProjectScripts
    {
        internal static string GetProjectsBySystemId()
        {
            return @"SELECT * FROM
                     PROJECT p
                     INNER JOIN PROJECT_SYSTEM ps ON p.PK_PROJECT_ID = ps.FK_PROJECT_ID
                     WHERE FK_SYSTEM_ID = @systemId";
        }

        internal static string GetProjectIdByName()
        {
            return @"select pk_project_id from project p where name_project = @nameProject";
        }
    }
}
