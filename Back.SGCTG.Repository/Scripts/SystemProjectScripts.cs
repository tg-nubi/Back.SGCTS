﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Back.SGCTS.Repository.Scripts
{
    public static class SystemProjectScripts
    {
        internal static string GetCurrentSystems()
        {
            return @"SELECT FK_SYSTEM_ID FROM PROJECT_SYSTEM up
                     WHERE FK_PROJECT_ID = @ProjectId";
        }

        internal static string Delete()
        {
            return @"DELETE FROM PROJECT_SYSTEM
                     WHERE FK_PROJECT_ID = @ProjectId
                     AND FK_SYSTEM_ID = @SystemId";
        }

        internal static string GetSistemsByProjects()
        {
            return @"SELECT *
                     FROM PROJECT_SYSTEM ps
                     inner join system s on s.pk_system_id = ps.fk_system_id
                     where s.fk_status_type_system_id <> 8 and  FK_PROJECT_ID = any (@projectIds)";
        }
    }
}
