﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Back.SGCTS.Repository.Scripts
{
    public static class DashboardScripts
    {
        public static string GetQuantityOfStatusBySystem()
        {
            return @"SELECT DISTINCT ST.STATUS_TYPE, COUNT(ST.STATUS_TYPE)
                    FROM CASES C
                    JOIN SCENARIO S ON S.PK_SCENARIO_ID = C.FK_SCENARIO_ID 
                    JOIN PROJECT P ON P.PK_PROJECT_ID = S.FK_PROJECT_SCENARIO_ID 
                    JOIN PROJECT_SYSTEM PS ON PS.FK_PROJECT_ID = P.PK_PROJECT_ID 
                    JOIN SYSTEM SY ON SY.PK_SYSTEM_ID = PS.FK_SYSTEM_ID
                    JOIN STATUS_TYPE ST ON ST.PK_STATUS_TYPE_ID = C.FK_STATUS_TYPE_CASES_ID
                    where ST.PK_STATUS_TYPE_ID not in (8) and SY.PK_SYSTEM_ID = @systemId 
                    GROUP BY STATUS_TYPE";
        }
        public static string GetExecutionsByStatusFromSystem()
        {
            return @"select E.DATE_END_EXECUTION as ExecutionDate, ST.STATUS_TYPE as Status, COUNT(ST.STATUS_TYPE) as Count
                    FROM EXECUTION E
                    JOIN CASES C ON C.PK_CASES_ID = E.FK_CASES_EXECUTION_ID 
                    JOIN SCENARIO S ON S.PK_SCENARIO_ID = C.FK_SCENARIO_ID 
                    JOIN PROJECT P ON P.PK_PROJECT_ID = S.FK_PROJECT_SCENARIO_ID 
                    JOIN PROJECT_SYSTEM PS ON PS.FK_PROJECT_ID = P.PK_PROJECT_ID 
                    JOIN STATUS_TYPE ST ON ST.PK_STATUS_TYPE_ID = E.FK_STATUS_TYPE_EXECUTION_ID
                    WHERE PS.fk_system_id = @systemId
                    and E.date_end_execution BETWEEN (CURRENT_DATE  - interval '7 day') AND (CURRENT_DATE + interval '1 day')
                    GROUP BY (E.DATE_END_EXECUTION, ST.STATUS_TYPE)
                    order by e.date_end_execution asc";
        }
    }
}
