﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Back.SGCTS.Repository.Scripts
{
    public class ExportScripts
    {
       
        internal static string GetExportWithSystem()
        {
            return @"select 
                    s.NAME_SYSTEM as nameSystem, 
                    p.NAME_PROJECT as nameProject, 
                    sc.NAME_SCENARIO as nameScenario, 
                    sc.DESCRIPTION_SCENARIO as descriptionScenario,
                    c.NAME_CASES as nameCase,
                    c.PRECONDITION_CASES as preconditionCase,
                    c.DATAMASS_CASES as datamassCase,
                    c.RESULT_EXPECTED_CASES as resultExpectedCase,
                    sct.NUMBER_STEP as numberStep,
                    sct.ACTION_STEP as actionStep,
                    sct.RESULT_EXPECTED_STEP as resultExpectedStep from " +
                    "\"system\""
                    + @"s
                    inner join PROJECT_SYSTEM ps on ps.FK_SYSTEM_ID = s.PK_SYSTEM_ID
                    inner join PROJECT p on p.PK_PROJECT_ID = ps.FK_PROJECT_ID
                    inner join SCENARIO sc on sc.FK_PROJECT_SCENARIO_ID = p.PK_PROJECT_ID
                    inner join CASES c on c.FK_SCENARIO_ID = sc.PK_SCENARIO_ID
                    inner join STEP_CT sct on sct.FK_CASES_STEP_ID = c.PK_CASES_ID
                    where s.PK_SYSTEM_ID = @SystemId";
        }

        internal static string GetExportWithProject()
        {
            return @"select 
                    s.NAME_SYSTEM as nameSystem, 
                    p.NAME_PROJECT as nameProject, 
                    sc.NAME_SCENARIO as nameScenario, 
                    sc.DESCRIPTION_SCENARIO as descriptionScenario,
                    c.NAME_CASES as nameCase,
                    c.PRECONDITION_CASES as preconditionCase,
                    c.DATAMASS_CASES as datamassCase,
                    c.RESULT_EXPECTED_CASES as resultExpectedCase,
                    sct.NUMBER_STEP as numberStep,
                    sct.ACTION_STEP as actionStep,
                    sct.RESULT_EXPECTED_STEP as resultExpectedStep from " +
                    "\"system\""
                    + @"s
                    inner join PROJECT_SYSTEM ps on ps.FK_SYSTEM_ID = s.PK_SYSTEM_ID
                    inner join PROJECT p on p.PK_PROJECT_ID = ps.FK_PROJECT_ID
                    inner join SCENARIO sc on sc.FK_PROJECT_SCENARIO_ID = p.PK_PROJECT_ID
                    inner join CASES c on c.FK_SCENARIO_ID = sc.PK_SCENARIO_ID
                    inner join STEP_CT sct on sct.FK_CASES_STEP_ID = c.PK_CASES_ID
                    where s.PK_SYSTEM_ID = @SystemId
                    and p.PK_PROJECT_ID = @ProjectId";
        }

        internal static string GetExportWithAllParameters()
        {
            return @"select 
                    s.NAME_SYSTEM as nameSystem, 
                    p.NAME_PROJECT as nameProject, 
                    sc.NAME_SCENARIO as nameScenario, 
                    sc.DESCRIPTION_SCENARIO as descriptionScenario,
                    c.NAME_CASES as nameCase,
                    c.PRECONDITION_CASES as preconditionCase,
                    c.DATAMASS_CASES as datamassCase,
                    c.RESULT_EXPECTED_CASES as resultExpectedCase,
                    sct.NUMBER_STEP as numberStep,
                    sct.ACTION_STEP as actionStep,
                    sct.RESULT_EXPECTED_STEP as resultExpectedStep from " +
                    "\"system\""
                    + @" s
                    inner join PROJECT_SYSTEM ps on ps.FK_SYSTEM_ID = s.PK_SYSTEM_ID
                    inner join PROJECT p on p.PK_PROJECT_ID = ps.FK_PROJECT_ID
                    inner join SCENARIO sc on sc.FK_PROJECT_SCENARIO_ID = p.PK_PROJECT_ID
                    inner join CASES c on c.FK_SCENARIO_ID = sc.PK_SCENARIO_ID
                    inner join STEP_CT sct on sct.FK_CASES_STEP_ID = c.PK_CASES_ID
                    where s.PK_SYSTEM_ID = @SystemId
                    and p.PK_PROJECT_ID = @ProjectId
                    and sc.PK_SCENARIO_ID = @ScenarioId";
        }

        internal static string VerifyExistentScenario()
        {
            return @"select sc.pk_scenario_id from " +
            "\"system\"" + @" s
            inner join PROJECT_SYSTEM ps on ps.FK_SYSTEM_ID = s.PK_SYSTEM_ID
            inner join PROJECT p on p.PK_PROJECT_ID = ps.FK_PROJECT_ID
            inner join SCENARIO sc on sc.FK_PROJECT_SCENARIO_ID = p.PK_PROJECT_ID
            where s.name_system = @nameSystem
            and p.name_project = @nameProject
            and sc.name_scenario = @nameScenario";
        }

        internal static string VerifyExistentCase()
        {
            return @"select c.pk_cases_id from " +
            "\"system\"" + @" s
            inner join PROJECT_SYSTEM ps on ps.FK_SYSTEM_ID = s.PK_SYSTEM_ID
            inner join PROJECT p on p.PK_PROJECT_ID = ps.FK_PROJECT_ID
            inner join SCENARIO sc on sc.FK_PROJECT_SCENARIO_ID = p.PK_PROJECT_ID
            inner join CASES c on c.FK_SCENARIO_ID = sc.PK_SCENARIO_ID
            where s.name_system = @nameSystem
            and p.name_project = @nameProject
            and sc.name_scenario = @nameScenario
            and c.name_cases = @nameCase";
        }
    }
}
