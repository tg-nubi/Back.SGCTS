﻿using AutoMapper;
using Back.SGCTS.ApplicationService.Extension;
using Back.SGCTS.Domain.Domain;
using Back.SGCTS.Domain.Enum;
using Back.SGCTS.DTO.DTO;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Back.SGCTS.Api.AutoMapper
{
    public class ProjectAutoMapper  :Profile
    {
        public ProjectAutoMapper()
        {
            CreateMap<ProjectDTO, Project>()
             .ForMember(d => d.StatusType, o => o.MapFrom(s => s.StatusType))
             .ForMember(d => d.StatusTypeId, o => o.MapFrom(s => EnumExtension.GetEnumValueFromDescription<StatusTypeEnum>(s.StatusType)));


            CreateMap<Project, ProjectDTO>()
                .ForMember(d => d.StatusType, o => o.MapFrom(s => EnumExtension.GetDescription(s.StatusType)));

        }
    }
}
