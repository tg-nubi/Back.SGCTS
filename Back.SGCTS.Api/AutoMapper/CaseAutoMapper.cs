﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using Back.SGCTS.ApplicationService.Extension;
using Back.SGCTS.Domain.Domain;
using Back.SGCTS.Domain.Enum;
using Back.SGCTS.DTO.DTO;
using Back.SGCTS.ApplicationService.Services;

namespace Back.SGCTS.Api.AutoMapper
{
    public class CaseAutoMapper : Profile
    {
        public CaseAutoMapper()
        {
            CreateMap<CaseDTO, Case>()
                .ForMember(d => d.StatusType, o => o.MapFrom(s => EnumExtension.GetEnumValueFromDescription<StatusTypeEnum>(s.StatusType)))
                .ForMember(d => d.StatusTypeId, o => o.MapFrom(s => EnumExtension.GetEnumValueFromDescription<StatusTypeEnum>(s.StatusType)))
                .ForMember(d => d.ScenarioId, o => o.MapFrom(s => s.ScenarioId))
                .ForMember(d => d.UserId, o => o.MapFrom(s => s.UserId));

            CreateMap<Case, CaseDTO>()
                .ForMember(d => d.StatusType, o => o.MapFrom(s => EnumExtension.GetDescription(s.StatusType)))
                .ForMember(d => d.ScenarioId, o => o.MapFrom(s => s.ScenarioId))
                .ForMember(d => d.UserId, o => o.MapFrom(s => s.UserId));

            CreateMap<Case, CaseResponseDTO>()
               .IncludeBase<Case,CaseDTO>()
               .ForMember(d => d.ScenarioName, o => o.MapFrom(s => s.Scenario.Name));

        }
    }
}
