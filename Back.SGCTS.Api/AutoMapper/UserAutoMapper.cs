﻿using AutoMapper;
using Back.SGCTS.ApplicationService.Extension;
using Back.SGCTS.Domain.Domain;
using Back.SGCTS.Domain.Enum;
using Back.SGCTS.DTO.DTO;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;


namespace Back.SGCTS.Api.AutoMapper
{
    public class UserAutoMapper : Profile
    {
        public UserAutoMapper()
        {
            CreateMap<UserDTO, User>()
            .ForMember(d => d.PermissionType, o => o.MapFrom(s => s.PermissionType))
            .ForMember(d => d.PermissionTypeId, o=> o.MapFrom(s => (int)EnumExtension.GetEnumValueFromDescription<PermissionTypeEnum>(s.PermissionType)))
            .ForMember(d => d.StatusType, o => o.MapFrom(s => s.StatusType))
            .ForMember(d => d.StatusTypeId, o => o.MapFrom(s => EnumExtension.GetEnumValueFromDescription<StatusTypeEnum>(s.StatusType)));
            // CreateMap<LoginDTO, User>();
            CreateMap<User, UserDTO>()
            .ForMember(d => d.PermissionType, o => o.MapFrom(s => EnumExtension.GetDescription(s.PermissionType)))
            .ForMember(d => d.StatusType, o => o.MapFrom(s => EnumExtension.GetDescription(s.StatusType)));
        }
    }
}
