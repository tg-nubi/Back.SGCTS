﻿using AutoMapper;
using Back.SGCTS.ApplicationService.Extension;
using Back.SGCTS.Domain.Domain;
using Back.SGCTS.Domain.Enum;
using Back.SGCTS.DTO.DTO;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Back.SGCTS.Api.AutoMapper
{
    public class ExecutionAutoMapper : Profile
    {
        public ExecutionAutoMapper()
        {
            CreateMap<ExecutionDTO, Execution>()
             .ForMember(d => d.StatusType, o => o.MapFrom(s => s.StatusType))
             .ForMember(d => d.StatusTypeId, o => o.MapFrom(s => EnumExtension.GetEnumValueFromDescription<StatusTypeEnum>(s.StatusType)));

            CreateMap<Execution, ExecutionDTO>()
             .ForMember(d => d.StatusType, o => o.MapFrom(s => s.StatusType.GetDescription()))
             .ForMember(d => d.UserName, o => o.MapFrom(s => $"{s.User.Name} {s.User.LastName}" ))
             .ForMember(d => d.DateShow, o => o.MapFrom(s => s.DateEnd.ToString("dd/MM/yyyy")));

        }
    }
}
