﻿using AutoMapper;
using Back.SGCTS.ApplicationService.Extension;
using Back.SGCTS.Domain.Domain;
using Back.SGCTS.Domain.Enum;
using Back.SGCTS.DTO.DTO;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Back.SGCTS.Api.AutoMapper
{
    public class TrashAutoMapper :Profile
    {
        public TrashAutoMapper()
        {
            CreateMap<Trash, TrashDTO>()
                .ForMember(d => d.Type, o => o.MapFrom(s => s.Type.GetDescription()));

            CreateMap<TrashDTO , Trash>()
               .ForMember(d => d.Type, o => o.MapFrom(s => EnumExtension.GetEnumValueFromDescription<ObjectTypeEnum>(s.Type)));
        }
    }
}
