﻿using AutoMapper;
using Back.SGCTS.ApplicationService.Extension;
using Back.SGCTS.Domain.Domain;
using Back.SGCTS.Domain.Enum;
using Back.SGCTS.DTO.DTO;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Back.SGCTS.Api.AutoMapper
{
    public class SystemAutoMapper : Profile
    {
        public SystemAutoMapper()
        {
            CreateMap<SystemDTO, Domain.Domain.System>()
              .ForMember(d => d.StatusType, o => o.MapFrom(s => s.StatusType))
              .ForMember(d => d.StatusTypeId, o => o.MapFrom(s => EnumExtension.GetEnumValueFromDescription<StatusTypeEnum>(s.StatusType)));
            
            CreateMap<Domain.Domain.System, SystemDTO>()
                .ForMember(d => d.StatusType, o => o.MapFrom(s => EnumExtension.GetDescription(s.StatusType)));
        }
    }
}
