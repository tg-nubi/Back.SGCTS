﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using Back.SGCTS.ApplicationService.Extension;
using Back.SGCTS.ApplicationService.Services;
using Back.SGCTS.Domain.Domain;
using Back.SGCTS.Domain.Enum;
using Back.SGCTS.DTO.DTO;

namespace Back.SGCTS.Api.AutoMapper
{
    public class StepCaseAutoMapper : Profile
    {
        public StepCaseAutoMapper()
        {
            CreateMap<StepCaseDTO, StepCase>()
              .ForMember(d => d.Case, o => o.MapFrom(s => new Case() { Id = s.CaseId }))
              .ForMember(d => d.CaseId, o => o.MapFrom(s => s.CaseId));

            CreateMap<StepCase, StepCaseDTO>();
        }
    }
}
