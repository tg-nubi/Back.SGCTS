﻿using AutoMapper;
using Back.SGCTS.ApplicationService.Extension;
using Back.SGCTS.Domain.Domain;
using Back.SGCTS.Domain.Enum;
using Back.SGCTS.DTO.DTO;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Back.SGCTS.Api.AutoMapper
{
    public class BugAutoMapper : Profile
    {
        public BugAutoMapper()
        {
            CreateMap<BugDTO, Bug>()
              .ForMember(d => d.Priority, o => o.MapFrom(s => EnumExtension.GetEnumValueFromDescription<PriorityTypeEnum>(s.Priority)))
              .ForMember(d => d.StatusType, o => o.MapFrom(s => EnumExtension.GetEnumValueFromDescription<StatusTypeEnum>(s.StatusType)));

            CreateMap<Bug, BugDTO>()
                .ForMember(d => d.StatusType, o => o.MapFrom(s => EnumExtension.GetDescription(s.StatusType)))
                .ForMember(d => d.Priority, o => o.MapFrom(s => EnumExtension.GetDescription(s.Priority)));

        }
    }
}
