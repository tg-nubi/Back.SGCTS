﻿using AutoMapper;
using Back.SGCTS.ApplicationService.Extension;
using Back.SGCTS.Domain.Domain;
using Back.SGCTS.DTO.Tree;
using System.Linq;

namespace Back.SGCTS.Api.AutoMapper
{
    public class TreeAutoMapper : Profile
    {
        public TreeAutoMapper()
        {
            CreateMap<Domain.Domain.System, TreeBaseDTO>().ConstructUsing(s => new SystemTreeDTO(s.Initials)).As<SystemTreeDTO>();

            CreateMap<Domain.Domain.System, SystemTreeDTO>()
                .ForMember(d => d.StatusType, o => o.MapFrom(s => EnumExtension.GetDescription(s.StatusType)))
                .ForMember(d => d.Children, o => o.MapFrom(s => s.Projects));

            CreateMap<Project, TreeBaseDTO>().ConstructUsing(s => new ProjectTreeDTO(s.Name)).As<ProjectTreeDTO>();
            CreateMap<Project, ProjectTreeDTO>()
                .ForMember(d => d.StatusType, o => o.MapFrom(s => EnumExtension.GetDescription(s.StatusType)))
                .ForMember(d => d.Children, o => o.MapFrom(s => s.Scenarios))
                .AfterMap((s, d) => d.Children = d.Children.Any() ? d.Children : null);

            CreateMap<Scenario, TreeBaseDTO>().ConstructUsing(s => new ScenarioTreeDTO(s.Name)).As<ScenarioTreeDTO>();
            CreateMap<Scenario, ScenarioTreeDTO>()
                .ForMember(d => d.StatusType, o => o.MapFrom(s => EnumExtension.GetDescription(s.StatusType)))
                .ForMember(d => d.Children, o => o.MapFrom(s => s.Cases))
                .AfterMap((s, d) => d.Children = d.Children.Any() ? d.Children : null);

            CreateMap<Case, TreeBaseDTO>().ConstructUsing(s => new CaseTreeDTO(s.Name)).As<CaseTreeDTO>();
            CreateMap<Case, CaseTreeDTO>()
                .ForMember(d => d.StatusType, o => o.MapFrom(s => EnumExtension.GetDescription(s.StatusType)));


        }
    }
}
