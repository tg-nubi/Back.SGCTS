﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using Back.SGCTS.ApplicationService.Extension;
using Back.SGCTS.ApplicationService.Services;
using Back.SGCTS.Domain.Domain;
using Back.SGCTS.Domain.Enum;
using Back.SGCTS.DTO.DTO;

namespace Back.SGCTS.Api.AutoMapper
{
    public class ScenarioAutoMapper : Profile
    {
        public ScenarioAutoMapper()
        {
            CreateMap<ScenarioDTO, Scenario>()
              .ForMember(d => d.StatusType, o => o.MapFrom(s => s.StatusType))
              .ForMember(d => d.StatusTypeId, o => o.MapFrom(s => EnumExtension.GetEnumValueFromDescription<StatusTypeEnum>(s.StatusType)))
              .ForMember(d => d.ProjectId, o => o.MapFrom(s => s.ProjectId));

            CreateMap<Scenario, ScenarioDTO>()
              .ForMember(d => d.StatusType, o => o.MapFrom(s => EnumExtension.GetDescription(s.StatusType)));
        }
    }
}
