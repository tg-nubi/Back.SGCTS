﻿using Back.SGCTS.ApplicationService.Factories.Reports;
using Back.SGCTS.ApplicationService.Interfaces;
using Back.SGCTS.ApplicationService.Services;
using Back.SGCTS.Domain.Interface.Services;
using Microsoft.Extensions.DependencyInjection;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Back.SGCTS.Api.IoC
{
    public class ServicesIoC
    {
        public static void RegisterServices(IServiceCollection services)
        {
            services.AddScoped<IBugAppService, BugAppService>();
            services.AddScoped<IProjectAppService, ProjectAppService>();
            services.AddScoped<IUserAppService,UserAppServices >();
            services.AddScoped<IAuthApplicationService, AuthApplicationService>();
            services.AddScoped<ISystemAppService, SystemAppService>();
            services.AddScoped<ICaseAppService, CaseAppService>();
            services.AddScoped<IScenarioAppService, ScenarioAppService>();
            services.AddScoped<IStepCaseAppService, StepCaseAppService>();
            services.AddScoped<IExecutionAppService, ExecutionApplicationService>();
            services.AddScoped<IExportApplicationServices, ExportApplicationService>();
            services.AddScoped<IEmailService, EmailService>();
            services.AddScoped<ITreeApplicationService, TreeApplicationService>();
            services.AddScoped<IReportApplicationServices, ReportApplicationService>();
            services.AddScoped<IDashboardApplicationService, DashboardApplicationService>();
            services.AddScoped<IMenuAppService, MenuAppService>();
            services.AddScoped<ISeverityAppService, SeverityAppService>();
            services.AddScoped<ITrashAppService, TrashAppService>();

            RegisterReportFactories(services);
        }
        private static void RegisterReportFactories(IServiceCollection services)
        {
            services.AddScoped<ReportFactory>();
            services.AddScoped<ReportByProject>();
            services.AddScoped<ReportByScenario>();
            services.AddScoped<ReportBySystem>();
        }
    }
}
