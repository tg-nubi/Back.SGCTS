﻿using Back.SGCTS.Domain.Interface;
using Back.SGCTS.Domain.Interface.Repository;
using Back.SGCTS.Repository.Repository;
using Microsoft.Extensions.DependencyInjection;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Back.SGCTS.Api.IoC
{
    public class RepositoryIoC
    {
        public static void RegisterServices(IServiceCollection services)
        {
            services.AddScoped<IBugRepository, BugDapperRepository>();
            services.AddScoped<IProjectRepository, ProjectDapperRepository>();
            services.AddScoped<IUserRepository, UserDapperRepository>();
            services.AddScoped<ISystemRepository, SystemDapperRepository>();
            services.AddScoped<ICaseRepository, CaseDapperRepository>();
            services.AddScoped<IScenarioRepository, ScenarioDapperRepository>();
            services.AddScoped<IStepCaseRepository, StepCaseDapperRepository>();
            services.AddScoped<IExecutionRepository, ExecutionDapperRepository>();
            services.AddScoped<IExportRepository, ExportDapperRepository>();
            services.AddScoped<IEmailTemplateRepository, EmailTemplateRepository>();
            services.AddScoped<IUserProjectRepository, UserProjectDapperRepository>();
            services.AddScoped<ISystemProjectRepository, SystemProjectDapperRepository>();
            services.AddScoped<IDashboardRepository, DashboardDapperRepository>();
            services.AddScoped<IMenuDapperRepository, MenuDapperRepository>();
            services.AddScoped<ISeverityDapperRepository, SeverityDapperRepository>();
            services.AddScoped<ITrashRepository, TrashRepository>();
        }
    }
}
