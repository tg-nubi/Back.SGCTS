﻿using Back.SGCTS.Domain.Enum;
using Back.SGCTS.Domain.Interface.Services;
using Microsoft.AspNetCore.Http;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Back.SGCTS.Api.Config
{
    public class MiddlewareTrash
    {
        private readonly RequestDelegate _next;

        public MiddlewareTrash(RequestDelegate next )
        {

            _next = next;
        }

        public async Task InvokeAsync(HttpContext context, ITrashAppService trashAppService)
        {
            if (HttpMethods.IsDelete(context.Request.Method))
            {
                if(IsTrashMethod(context.Request.Path, out var objectTypeEnum))
                {
                    
                    trashAppService.Insert(new Domain.Domain.Trash()
                    {
                        ObjectId = Convert.ToInt32(context.Request.Path.Value.Split('/').LastOrDefault()),
                        Type = objectTypeEnum,
                        UserName = context.User.Claims.First(s => s.Type == "userName").Value
                    });
                }
            }
            await _next(context);
        }
        private bool IsTrashMethod(string route, out ObjectTypeEnum objectTypeEnum)
        {
            var isTrashMethod = true;
            switch (route.ToLower())
            {
                case string s when s.Contains("api/system"):
                    objectTypeEnum = ObjectTypeEnum.System;
                    break;
                case string s when s.Contains("api/project"):
                    objectTypeEnum = ObjectTypeEnum.Project;
                    break;
                case string s when s.Contains("api/scenario"):
                    objectTypeEnum = ObjectTypeEnum.Scenario;
                    break;
                case string s when s.Contains("api/case"):
                    objectTypeEnum = ObjectTypeEnum.Case;
                    break;
                default :
                    objectTypeEnum = default;
                    isTrashMethod = false;
                    break;
            }
            return isTrashMethod;
        }
    }
}
