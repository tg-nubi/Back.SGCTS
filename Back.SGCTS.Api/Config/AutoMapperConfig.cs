﻿using AutoMapper;
using Back.SGCTS.Api.AutoMapper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Back.SGCTS.Api.Config
{
    public static class AutoMapperConfig
    {
        public static IMapper Configure()
        {
            var mappingConfig = new MapperConfiguration(mc =>
              {
                  mc.AddProfile(new UserAutoMapper());
                  mc.AddProfile(new SystemAutoMapper());
                  mc.AddProfile(new BugAutoMapper());
                  mc.AddProfile(new ProjectAutoMapper());
                  mc.AddProfile(new CaseAutoMapper());
                  mc.AddProfile(new ScenarioAutoMapper());
                  mc.AddProfile(new ExecutionAutoMapper());
                  mc.AddProfile(new StepCaseAutoMapper());
                  mc.AddProfile(new TreeAutoMapper());
                  mc.AddProfile(new TrashAutoMapper());
              });
            IMapper mapper = mappingConfig.CreateMapper();
            return mapper;
        }
    }
}
