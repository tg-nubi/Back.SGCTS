﻿using Back.SGCTS.ApplicationService.Exceptions;
using Back.SGCTS.DTO.Base;
using Microsoft.AspNetCore.Http;
using Newtonsoft.Json;
using Serilog;
using System;
using System.IO;
using System.Net;
using System.Threading.Tasks;

namespace Back.SGCTS.Api.Config
{
    public class MiddlewareError
    {
        private readonly RequestDelegate _next;

        public MiddlewareError(RequestDelegate next)
        {
            _next = next;
        }

        public async Task InvokeAsync(HttpContext context)
        {
            try
            {
                await _next(context);
            }
            catch (Exception ex)
            {
                await HandleExceptionAsync(context, ex);
            }
        }
        private Task HandleExceptionAsync(HttpContext context, Exception exception)
        {
            HttpStatusCode code;

            if (exception is NotFoundException)
                code = HttpStatusCode.NotFound;
            else {
                code = HttpStatusCode.InternalServerError;
                Log.Error(exception, "Error at Request");
            }

            var response = new BaseResponseDTO();
            response.AddError(exception.Message);
     
            context.Response.ContentType = "application/json";
            context.Response.StatusCode = (int)code;
            return context.Response.WriteAsync(JsonConvert.SerializeObject(response));
        }
    }
}
