﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Back.SGCTS.ApplicationService.Interfaces;
using Back.SGCTS.DTO.Base;
using Back.SGCTS.DTO.Dashboard;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace Back.SGCTS.Api.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    [Authorize]
    [Produces("application/json")]
    public class DashboardController : BaseControler
    {
        private readonly IDashboardApplicationService _dashboardApplicationService;
        public DashboardController(IDashboardApplicationService dashboardApplicationService,
                                   IHttpContextAccessor contextAccessor) : base(contextAccessor)
        {
            _dashboardApplicationService = dashboardApplicationService;
        }
        [HttpGet]
        public ActionResult Get()
        {
            var userId = GetUserId();
            var dash = _dashboardApplicationService.GetGeneralDashboard(userId);
            return Ok(new BaseResponseEntityDTO<IList<GeneralDashboardDTO>>(dash));
        }
    }
}