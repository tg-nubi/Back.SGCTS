﻿using AutoMapper;
using Back.SGCTS.Domain.Domain;
using Back.SGCTS.Domain.Interface.Services;
using Back.SGCTS.DTO.Base;
using Back.SGCTS.DTO.DTO;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Serilog;
using System.Collections.Generic;

namespace Back.SGCTS.Api.Controllers
{
    [ApiController]
    [Authorize]
    [Route("api/[controller]")]
    [Produces("application/json")]
    public class ScenarioController : BaseControler
    {
        private readonly IMapper _mapper;
        private readonly IScenarioAppService _scenarioAppService;
        public ScenarioController(IMapper mapper,
                              IScenarioAppService ScenarioAppService,
                              IHttpContextAccessor httpContextAccessor) : base(httpContextAccessor)
        {
            _mapper = mapper;
            _scenarioAppService = ScenarioAppService;
        }
        [HttpGet]
        [Route("GetByProject/{ProjectId}")]
        public ActionResult<BaseResponseEntityDTO<IEnumerable<ScenarioDTO>>> GetByProject(int projectId)
        {
            Log.Information("Recived request GetAll");

            var scenarios = _scenarioAppService.GetByProjectId(projectId);
            return Ok(new BaseResponseEntityDTO<IEnumerable<ScenarioDTO>>(_mapper.Map<IEnumerable<ScenarioDTO>>(scenarios)));
        }

        [HttpGet]
        [Route("{scenarioId}")]
        public ActionResult<BaseResponseEntityDTO<ScenarioDTO>> GetById(int scenarioId)
        {
            Log.Information("Recived request SelectById: {@Id}", scenarioId);

            var scenario = _scenarioAppService.GetById(scenarioId);
            return Ok(new BaseResponseEntityDTO<ScenarioDTO>(_mapper.Map<ScenarioDTO>(scenario)));
        }

        [HttpPost]
        public ActionResult<BaseResponseEntityDTO<ScenarioDTO>> Insert(ScenarioDTO scenarioDTO)
        {
            Log.Information("Recived request Insert: {@scenario}", scenarioDTO);
            var scenario = _mapper.Map<Scenario>(scenarioDTO);

            scenario.Id =  _scenarioAppService.Insert(scenario);
            return StatusCode(StatusCodes.Status201Created, new BaseResponseEntityDTO<ScenarioDTO>(_mapper.Map<ScenarioDTO>(scenario)));
        }

        [HttpPut]
        public IActionResult Update(ScenarioDTO scenarioDTO)
        {
            Log.Information("Recived request Update: {@scenario}", scenarioDTO);
            var scenario = _mapper.Map<Scenario>(scenarioDTO);

            bool sucess = _scenarioAppService.Update(scenario);
            if (sucess)
                return StatusCode(StatusCodes.Status200OK);
            else
                return StatusCode(StatusCodes.Status500InternalServerError);
        }
        [HttpDelete]
        [Route("{scenarioId}")]
        public ActionResult<BaseResponseEntityDTO<CaseResponseDTO>> Delete(int scenarioId)
        {
            Log.Information("Recived request Delete: {@Id}", scenarioId);
            var result = _scenarioAppService.UpdateStatus(scenarioId, Domain.Enum.StatusTypeEnum.Deletado);

            return Ok(new BaseResponseEntityDTO<bool>(result));
        }
    }
}
