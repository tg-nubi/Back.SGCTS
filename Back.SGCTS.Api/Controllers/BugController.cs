﻿using AutoMapper;
using Back.SGCTS.Domain.Domain;
using Back.SGCTS.Domain.Interface.Services;
using Back.SGCTS.DTO.Base;
using Back.SGCTS.DTO.DTO;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Serilog;
using System.Collections.Generic;

namespace Back.SGCTS.Api.Controllers
{
    [ApiController]
    [Authorize]
    [Route("api/[controller]")]
    [Produces("application/json")]
    public class BugController : BaseControler
    {
        private readonly IMapper _mapper;
        private readonly IBugAppService _bugAppService;
        public BugController(IMapper mapper,
                              IBugAppService bugAppService,
                              IHttpContextAccessor httpContextAccessor) : base(httpContextAccessor)
        {
            _mapper = mapper;
            _bugAppService = bugAppService;
        }

        [HttpGet]
        [Route("All")]
        public ActionResult<BaseResponseEntityDTO<IEnumerable<BugDTO>>> GetAll()
        {
            Log.Information("Recived request GetAll");

            var bugs = _bugAppService.GetAll();
            return Ok(new BaseResponseEntityDTO<IEnumerable<BugDTO>>(_mapper.Map<IEnumerable<BugDTO>>(bugs)));
        }

        [HttpGet]
        [Route("{bugId}")]
        public ActionResult<BaseResponseEntityDTO<Bug>> GetById(int bugId)
        {
            Log.Information("Recived request SelectById: {@Bug}", bugId);

            var bug = _bugAppService.GetById(bugId);
            return Ok( new BaseResponseEntityDTO<Bug>(bug));
        }

        [HttpGet]
        [Route("user")]
        public ActionResult<BaseResponseEntityDTO<IEnumerable<BugDTO>>> GetByUserId()
        {
            var userId = GetUserId();

            Log.Information("Recived request SelectByUserId: {@User}", userId);

            var bugs = _bugAppService.GetByUserId(userId);
            return Ok(new BaseResponseEntityDTO<IEnumerable<BugDTO>>(_mapper.Map<IEnumerable<BugDTO>>(bugs)));
        }

        [HttpPost]
        public IActionResult Insert(BugDTO bugDTO)
        {
            Log.Information("Recived request Insert: {@Bug}", bugDTO);
            var bug = _mapper.Map<Bug>(bugDTO);

            _bugAppService.Insert(bug);
            return StatusCode(StatusCodes.Status201Created);
        }

        [HttpPut]
        public IActionResult Update (BugDTO bugDTO)
        {
            Log.Information("Recived request Update: {@Bug}", bugDTO);
            var bug = _mapper.Map<Bug>(bugDTO);

            bool sucess = _bugAppService.Update(bug);
            if (sucess)
                return StatusCode(StatusCodes.Status200OK);
            else
                return StatusCode(StatusCodes.Status500InternalServerError);
        }
    }
}
