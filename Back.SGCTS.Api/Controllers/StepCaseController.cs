﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using Back.SGCTS.Domain.Domain;
using Back.SGCTS.Domain.Interface.Services;
using Back.SGCTS.DTO.Base;
using Back.SGCTS.DTO.DTO;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Serilog;

namespace Back.SGCTS.Api.Controllers
{
    [ApiController]
    [Authorize]
    [Route("api/[controller]")]
    [Produces("application/json")]
    public class StepCaseController : BaseControler
    {
        private readonly IMapper _mapper;
        private readonly IStepCaseAppService _stepCaseAppService;
        public StepCaseController(IMapper mapper,
                              IStepCaseAppService StepCaseAppService,
                              IHttpContextAccessor httpContextAccessor) : base(httpContextAccessor)
        {
            _mapper = mapper;
            _stepCaseAppService = StepCaseAppService;
        }

        [HttpGet]
        [Route("{stepCaseId}")]
        public ActionResult<BaseResponseEntityDTO<StepCase>> GetById(int StepCaseId)
        {
            Log.Information("Recived request SelectById: {@Id}", StepCaseId);

            var stepCase = _stepCaseAppService.GetById(StepCaseId);
            return Ok(new BaseResponseEntityDTO<StepCase>(stepCase));
        }
        [HttpGet]
        [Route("byCase/{caseId}")]
        public ActionResult<BaseResponseEntityDTO<StepCase>> GetByCaseId(int caseId)
        {
            Log.Information("Recived request SelectById: {@Id}", caseId);

            var stepCase = _stepCaseAppService.GetByCaseId(caseId);
            return Ok(new BaseResponseEntityDTO<IEnumerable<StepCaseDTO>>(_mapper.Map<IEnumerable<StepCaseDTO>>(stepCase)));
        }

        [HttpPost]
        public IActionResult Insert(StepCaseDTO stepCaseDTO)
        {
            Log.Information("Recived request Insert: {@stepCase}", stepCaseDTO);
            var stepCase = _mapper.Map<StepCase>(stepCaseDTO);

            _stepCaseAppService.Insert(stepCase);
            return StatusCode(StatusCodes.Status201Created);
        }

        [HttpPut]
        public IActionResult Update(StepCaseDTO stepCaseDTO)
        {
            Log.Information("Recived request Update: {@stepCase}", stepCaseDTO);
            var stepCase = _mapper.Map<StepCase>(stepCaseDTO);

            bool sucess = _stepCaseAppService.Update(stepCase);
            if (sucess)
                return StatusCode(StatusCodes.Status200OK);
            else
                return StatusCode(StatusCodes.Status500InternalServerError);
        }

        [HttpDelete]
        public IActionResult Delete(int StepCaseId)
        {
            Log.Information("Recived delete Delete: {@Id}", StepCaseId);

            bool sucess = _stepCaseAppService.Delete(StepCaseId);
            if (sucess)
                return StatusCode(StatusCodes.Status200OK);
            else
                return StatusCode(StatusCodes.Status500InternalServerError);
        }
    }
}
