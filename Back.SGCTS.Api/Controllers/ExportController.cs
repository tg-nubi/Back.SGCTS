﻿using AutoMapper;
using Back.SGCTS.ApplicationService.Interfaces;
using Back.SGCTS.Domain.Domain;
using Back.SGCTS.DTO.Exports;
using ExcelDataReader;
using iTextSharp.text;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using OfficeOpenXml;
using Serilog;
using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Transactions;
using System.Web;
using System.Web.Http;


namespace Back.SGCTS.Api.Controllers
{
    [ApiController]
    [Authorize]
    [Route("api/[controller]")]
    [Produces("application/json")]
    public class ExportController : BaseControler
    {
        private readonly IMapper _mapper;
        private readonly IExportApplicationServices _exportApplicationServices;

        public ExportController(IMapper mapper,
                                IExportApplicationServices exportApplicationServices,
                                IHttpContextAccessor httpContextAcessor) : base(httpContextAcessor)
        {
            _mapper = mapper;
            _exportApplicationServices = exportApplicationServices;
        }

        [HttpGet]
        [Route("")]
        public async Task<FileResult> CreateDataToExport([FromQuery] int? systemId, [FromQuery] int? projectId, [FromQuery] int? scenarioId)
        {
            Log.Information("Recived request CreateDataToExport: {@System}, {@Project}, {@Scenario}", systemId, projectId, scenarioId);

            RequestDataExport requestDataExport = new RequestDataExport(systemId, projectId, scenarioId);

            var export = _exportApplicationServices.GenerateDataForExport(requestDataExport);

            Response.ContentType = "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet";

            var fileContentResult = new FileContentResult(export, "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet"){};

            return fileContentResult;
        }

        [HttpPost]
        [Route("")]
        public async Task<string> ReadExcel()
        {
            var userId = GetUserId();
            var httpRequest = HttpContext.Request.Form.Files.GetFile("sheet");

            var stream = new MemoryStream();
            await httpRequest.CopyToAsync(stream);

            using (TransactionScope scope = new TransactionScope())
            {
                _exportApplicationServices.ImportData(stream, userId);
                scope.Complete();
            }

            return "Sucesso";
        }
    }
}
