﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;
using Back.SGCTS.Domain.Enum;
using Back.SGCTS.Domain.Interface.Services;
using Back.SGCTS.Domain.ValueObjects;
using Back.SGCTS.DTO.Base;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace Back.SGCTS.Api.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class ConfigController : BaseControler
    {
        private readonly IMenuAppService _menuAppService;
        public ConfigController(IMenuAppService menuAppService,
                                IHttpContextAccessor contextAccessor) : base(contextAccessor)
        {
            _menuAppService = menuAppService;
        }
        
        [HttpGet]
        [Route("menu")]
        public ActionResult GetMenu()
        {
            PermissionTypeEnum permission = Enum.Parse<PermissionTypeEnum>(HttpContext.User.Claims.First(s => s.Type == ClaimTypes.Role).Value);

            var menu = _menuAppService.GetMenusByPermission(permission);
            
            return Ok( new BaseResponseEntityDTO<IEnumerable<Menu>>(menu)); 
        }
    }
}