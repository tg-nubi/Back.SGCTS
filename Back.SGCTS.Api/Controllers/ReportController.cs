﻿using Back.SGCTS.ApplicationService.Interfaces;
using Back.SGCTS.DTO.Reports;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Serilog;
using System.Linq;

namespace Back.SGCTS.Api.Controllers
{
    [ApiController]
    [Authorize]
    [Route("api/[controller]")]
    [Produces("application/json")]
    public class ReportController : BaseControler
    {
        private readonly IReportApplicationServices _reportApplicationService;

        public ReportController(IReportApplicationServices reportApplicationService,
                                IHttpContextAccessor httpContextAcessor) : base(httpContextAcessor)
        {
            _reportApplicationService = reportApplicationService;
        }

        [HttpGet]
        [Route("system/{systemId}")]
        public ActionResult CreateReportBySystemId(int systemId)
        {
            Log.Information("Recived request CreateReportBySystemId: {@System}", systemId);

            var report = _reportApplicationService.GenerateReport(new RequestReportBySystem(systemId));

            Response.ContentType = "aplication/pdf";
            Response.Headers.Append("content-disposition", "attachment; filename=relatorioTestes.pdf");

            return report;
        }

        [HttpGet]
        [Route("system/{systemId}/project/{projectId}")]
        public ActionResult CreateReportByProjectId(int systemId, int projectId)
        {
            Log.Information("Recived request CreateReportByProjectId: {@Project}", projectId);

            var report = _reportApplicationService.GenerateReport(new RequestReportByProject(systemId, projectId));

            Response.ContentType = "aplication/pdf";
            Response.Headers.Append("content-disposition", "attachment; filename=relatorioTestes.pdf");

            return report;
        }

        [HttpGet]
        [Route("system/{systemId}/project/{projectId}/scenario/{scenarioId}")]
        public ActionResult CreateReportByScenarioId(int systemId, int projectId, int scenarioId)
        {
            Log.Information("Recived request CreateReportByScenarioId: {@Scenario}", scenarioId);

            var report = _reportApplicationService.GenerateReport(new RequestReportByScenaio(systemId, projectId, scenarioId));

            Response.ContentType = "aplication/pdf";
            Response.Headers.Append("content-disposition", "attachment; filename=relatorioTestes.pdf");

            return report;
        }
    }
}
