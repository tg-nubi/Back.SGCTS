﻿using AutoMapper;
using Back.SGCTS.Domain.Interface.Services;
using Back.SGCTS.DTO.Base;
using Back.SGCTS.DTO.DTO;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Serilog;
using System.Collections.Generic;

namespace Back.SGCTS.Api.Controllers
{
    [ApiController]
    [Authorize]
    [Route("api/[controller]")]
    [Produces("application/json")]
    public class SystemController : BaseControler
    {
        private readonly IMapper _mapper;
        private readonly ISystemAppService _systemAppService;
        public SystemController(IMapper mapper,
                              ISystemAppService SystemAppService,
                              IHttpContextAccessor httpContextAccessor) : base(httpContextAccessor)
        {
            _mapper = mapper;
            _systemAppService = SystemAppService;
        }

        [HttpGet]
        [Route("All")]
        public ActionResult<BaseResponseEntityDTO<SystemDTO>> GetAll()
        {
            Log.Information("Recived request GetAll");

            var system = _systemAppService.GetAll();
            return Ok(new BaseResponseEntityDTO<IEnumerable<SystemDTO>> (_mapper.Map<IEnumerable<SystemDTO>>(system)));
        }

        [HttpGet]
        [Route("{systemId}")]
        public ActionResult<BaseResponseEntityDTO<SystemDTO>> GetById(int systemId)
        {
            Log.Information("Recived request SelectById: {@Id}", systemId);

            var system = _systemAppService.GetById(systemId);
            return Ok(new BaseResponseEntityDTO<SystemDTO>(_mapper.Map<SystemDTO>(system)));
        }

        [HttpPost]
        public IActionResult Insert(SystemDTO systemDTO)
        {
            Log.Information("Recived request Insert: {@system}", systemDTO);
            var system = _mapper.Map<Domain.Domain.System>(systemDTO);

            system.Id = _systemAppService.Insert(system);
            return StatusCode(StatusCodes.Status201Created,new BaseResponseEntityDTO<SystemDTO>(_mapper.Map<SystemDTO>(system)));
        }

        [HttpPut]
        public IActionResult Update(SystemDTO systemDTO)
        {
            Log.Information("Recived request Update: {@system}", systemDTO);
            var system = _mapper.Map<Domain.Domain.System>(systemDTO);

            bool sucess = _systemAppService.Update(system);
            if (sucess)
                return StatusCode(StatusCodes.Status200OK);
            else
                return StatusCode(StatusCodes.Status500InternalServerError);
        }
        [HttpDelete]
        [Route("{systemId}")]
        public ActionResult<BaseResponseEntityDTO<CaseResponseDTO>> Delete(int systemId)
        {
            Log.Information("Recived request Delete: {@Id}", systemId);
            var result = _systemAppService.UpdateStatus(systemId, Domain.Enum.StatusTypeEnum.Deletado);

            return Ok(new BaseResponseEntityDTO<bool>(result));
        }
    }
}
