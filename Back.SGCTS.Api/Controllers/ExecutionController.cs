using AutoMapper;
using Back.SGCTS.Domain.Domain;
using Back.SGCTS.Domain.Interface.Services;
using Back.SGCTS.DTO.Base;
using Back.SGCTS.DTO.DTO;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Serilog;
using System;
using System.Collections.Generic;

namespace Back.SGCTS.Api.Controllers
{
    [ApiController]
    [Authorize]
    [Route("api/[controller]")]
    [Produces("application/json")]
    public class ExecutionController : BaseControler
    {
        private readonly IMapper _mapper;
        private readonly IExecutionAppService _executionAppService;
        public ExecutionController(IMapper mapper,
                                  IExecutionAppService executionAppService,
                                  IHttpContextAccessor httpContextAccessor) : base(httpContextAccessor)
        {
            _mapper = mapper;
            _executionAppService = executionAppService;
        }

        [HttpGet]
        [Route("{Id}")]
        public ActionResult<BaseResponseEntityDTO<ExecutionDTO>> GetById(int Id)
        {
            Log.Information("Recived request SelectById: {@Id}", Id);

            var execution = _executionAppService.GetById(Id);
            return Ok(new BaseResponseEntityDTO<Execution>(execution));
        }

        [HttpPost]
        public IActionResult Insert(ExecutionDTO createExecutionDTO)
        {
            createExecutionDTO.UserId = GetUserId();
            createExecutionDTO.DateEnd = DateTime.Now;
            Log.Information("Recived request Insert: {@Execution}", createExecutionDTO);
            var execution = _mapper.Map<Execution>(createExecutionDTO);

            createExecutionDTO.Id = _executionAppService.Insert(execution);
            return StatusCode(StatusCodes.Status201Created, new BaseResponseEntityDTO<ExecutionDTO>(createExecutionDTO));
        }

        [HttpPut]
        public IActionResult Update(ExecutionDTO updateExecutionDTO)
        {
            updateExecutionDTO.UserId = GetUserId();
            Log.Information("Recived request UpdateUser: {@User}", updateExecutionDTO);
            var execution = _mapper.Map<Execution>(updateExecutionDTO);

            bool sucess = _executionAppService.Update(execution);
            if (sucess)
                return StatusCode(StatusCodes.Status200OK);
            else
                return StatusCode(StatusCodes.Status500InternalServerError);
        }

        [HttpGet]
        [Route("LoadExecution")]
        public ActionResult<BaseResponseEntityDTO<LoadExecutionDTO>> LoadExecution(int caseId, int executionId)
        {
            Log.Information("Recived request LoadExecution: {@caseId}", caseId);
            if (executionId != 0)
                caseId = _executionAppService.GetById(executionId).CaseId;

            var loadExecution = _executionAppService.LoadExecution(caseId);
            return Ok(new BaseResponseEntityDTO<LoadExecutionDTO>(loadExecution));
        }

        [HttpGet]
        [Route("ByCase/{caseId}")]
        public ActionResult<BaseResponseEntityDTO<ExecutionDTO>> GetExecutionByCase(int caseId)
        {
            Log.Information("Recived request GetExecutionByCase: {@caseId}", caseId);

            var executions = _executionAppService.GetExecutionByCase(caseId);
            var executionDTO = _mapper.Map<IEnumerable<ExecutionDTO>>(executions);

            return Ok(new BaseResponseEntityDTO<IEnumerable<ExecutionDTO>>(executionDTO));
        }
    }
}
