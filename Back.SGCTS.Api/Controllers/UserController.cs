﻿using AutoMapper;
using Back.SGCTS.ApplicationService.Exceptions;
using Back.SGCTS.ApplicationService.Interfaces;
using Back.SGCTS.Domain.Domain;
using Back.SGCTS.Domain.Enum;
using Back.SGCTS.Domain.Interface.Services;
using Back.SGCTS.DTO.Auth;
using Back.SGCTS.DTO.Base;
using Back.SGCTS.DTO.DTO;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Serilog;
using System.Collections.Generic;

namespace Back.SGCTS.Api.Controllers
{
    [ApiController]
    [Authorize]
    [Route("api/[controller]")]
    [Produces("application/json")]
    public class UserController : BaseControler
    {
        private readonly IMapper _mapper;
        private readonly IUserAppService _userAppService;
        private readonly IAuthApplicationService _authApplicationService;
        public UserController(IMapper mapper,
                              IUserAppService userAppService,
                              IAuthApplicationService authApplicationService,
                              IHttpContextAccessor httpContextAccessor) : base(httpContextAccessor)
        {
            _mapper = mapper;
            _userAppService = userAppService;
            _authApplicationService = authApplicationService;
        }
        [HttpGet]
        [Route("All")]
        public ActionResult<BaseResponseEntityDTO<UserDTO>> GetAll()
        {
            Log.Information("Recived request GetAll");

            var users = _userAppService.GetAll();
            return Ok(new BaseResponseEntityDTO<IEnumerable<UserDTO>>(_mapper.Map<IEnumerable<UserDTO>>(users)));
        }
        [HttpGet]
        [Route("{userId}")]
        public ActionResult<BaseResponseEntityDTO<User>> GetById(int UserId)
        {
            Log.Information("Recived request SelectById: {@Id}", UserId);

            var user = _userAppService.GetById(UserId);
            return Ok(new BaseResponseEntityDTO<User>(user));
        }
        [HttpPost]
        public ActionResult<BaseResponseEntityDTO<UserDTO>> CreateUser(UserDTO createUserDTO)
        {
            Log.Information("Recived request CreateUser: {@User}", createUserDTO);
            var user = _mapper.Map<User>(createUserDTO);

            createUserDTO.Id = _userAppService.Insert(user);
            createUserDTO.Login = user.Login;
            var response = new BaseResponseEntityDTO<UserDTO>(createUserDTO);
            return StatusCode(StatusCodes.Status201Created, response);
        }

        [HttpPut]
        public IActionResult UpdateUser(UserDTO updateUserDTO)
        {
            Log.Information("Recived request UpdateUser: {@User}", updateUserDTO);
            var user = _mapper.Map<User>(updateUserDTO);

            bool sucess = _userAppService.Update(user);
            if (sucess)
                return StatusCode(StatusCodes.Status200OK);
            else
                return StatusCode(StatusCodes.Status500InternalServerError);
        }
        [HttpDelete]
        [Route("{userId}")]
        public IActionResult DeleteUser(int userId)
        {
            Log.Information("Recived request DeleteUser: {UserId}", userId);
            var user = _userAppService.GetById(userId);
            user.StatusType = StatusTypeEnum.Inativo;
            bool sucess = _userAppService.Update(user);
            if (sucess)
                return StatusCode(StatusCodes.Status200OK);
            else
                return StatusCode(StatusCodes.Status500InternalServerError);
        }

        [HttpPost]
        [AllowAnonymous]
        [Route("ResetPassword")]
        public IActionResult ResetPassword(string login)
        {
            var email = _userAppService.ResetPassword(login);
            return Ok(email);
        }

        [HttpPost]
        [AllowAnonymous]
        [Route("CreatePassword")]
        public IActionResult CreatePassword(CreatePasswordDTO dto)
        {
            if (dto.NewPassword != dto.ConfirmPassword)
                throw new NotFoundException("Nova senha não conhecide com a confirmação");

            if (_authApplicationService.AuthUser(new AuthDTO() { User = dto.User, Password = dto.CurrentPassword }) == null)
                throw new NotFoundException("Senha anterior inválida");

            _userAppService.CreatePassword(dto.User, dto.NewPassword);

            return Ok();
        }
    }
}