﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Back.SGCTS.ApplicationService.Interfaces;
using Back.SGCTS.DTO.Base;
using Back.SGCTS.DTO.DTO;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace Back.SGCTS.Api.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class AuthController : BaseControler
    {
        private readonly IAuthApplicationService _authApplicationService;
        public AuthController(IHttpContextAccessor contextAccessor,
                              IAuthApplicationService authApplicationService) : base(contextAccessor)
        {
            _authApplicationService = authApplicationService;
        }
        [HttpPost]
        public ActionResult<BaseResponseEntityDTO<AuthResponseDTO>> UserAuth([FromBody] AuthDTO authDTO)
        {
            var result = _authApplicationService.AuthUser(authDTO);
            var response = new BaseResponseEntityDTO<AuthResponseDTO>(result);
            if (result == null)
            {
                response.AddError("Login ou Senha Inválidos");
                return StatusCode(403);
            }
              
            
            return Ok(response);
        }
    }
}