﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Serilog.Context;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Back.SGCTS.Api.Controllers
{
    public class BaseControler : ControllerBase
    {
        public BaseControler(IHttpContextAccessor contextAccessor)
        {
            LogContext.PushProperty("Path", contextAccessor?.HttpContext?.Request?.Path);
            LogContext.PushProperty("RequestId", contextAccessor?.HttpContext?.TraceIdentifier);
            LogContext.PushProperty("CorrelationId", Guid.NewGuid().ToString());
            LogContext.PushProperty("MachineName", Environment.MachineName);
        }
        protected int GetUserId() =>
            Convert.ToInt32(HttpContext.User.Claims.First(s => s.Type == "userId").Value);
    }
}
