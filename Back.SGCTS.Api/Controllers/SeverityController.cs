﻿using AutoMapper;
using Back.SGCTS.ApplicationService.Interfaces;
using Back.SGCTS.Domain.Interface.Services;
using Back.SGCTS.Domain.ValueObjects;
using Back.SGCTS.DTO.Base;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Serilog;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Back.SGCTS.Api.Controllers
{
    [ApiController]
    [Authorize]
    [Route("api/[controller]")]
    [Produces("application/json")]
    public class SeverityController : BaseControler
    {
        private readonly IMapper _mapper;
        private readonly ISeverityAppService _severityAppService;

        public SeverityController(IMapper mapper,
                                  ISeverityAppService severityAppService,
                                  IHttpContextAccessor httpContextAccessor) : base(httpContextAccessor)
        {
            _mapper = mapper;
            _severityAppService = severityAppService;
        }
        [HttpGet]
        [Route("All")]
        public ActionResult<BaseResponseEntityDTO<IEnumerable<SeverityType>>> GetAllSeverity()
        {
            Log.Information("Recived request GetAll");

            var serverity = _severityAppService.GetAll();
            return Ok(new BaseResponseEntityDTO<IEnumerable<SeverityType>>(_mapper.Map<IEnumerable<SeverityType>>(serverity)));

        }

        [HttpPost]
        public ActionResult<BaseResponseEntityDTO<SeverityType>> CreateSeverityType(SeverityType severityType)
        {
            Log.Information("Recived request CreateSeverityType: {@severityType}", severityType);
            if (string.IsNullOrWhiteSpace(severityType.Type))
                return StatusCode(StatusCodes.Status204NoContent);

            severityType.Id = _severityAppService.Insert(severityType);
            var response = new BaseResponseEntityDTO<SeverityType>(severityType);
            return StatusCode(StatusCodes.Status201Created, response);
        }
    }
}
