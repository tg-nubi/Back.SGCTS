﻿using AutoMapper;
using Back.SGCTS.Domain.Domain;
using Back.SGCTS.Domain.Interface.Services;
using Back.SGCTS.DTO.Base;
using Back.SGCTS.DTO.DTO;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Serilog;
using System.Collections.Generic;

namespace Back.SGCTS.Api.Controllers
{
    [ApiController]
    [Authorize]
    [Route("api/[controller]")]
    [Produces("application/json")]
    public class ProjectController : BaseControler
    {
        private readonly IMapper _mapper;
        private readonly IProjectAppService _projectAppService;
        public ProjectController(IMapper mapper,
                              IProjectAppService ProjectBugAppService,
                              IHttpContextAccessor httpContextAccessor) : base(httpContextAccessor)
        {
            _mapper = mapper;
            _projectAppService = ProjectBugAppService;
        }
        [HttpGet]
        [Route("All")]
        public ActionResult<BaseResponseEntityDTO<IEnumerable<ProjectDTO>>> GetAll()
        {
            Log.Information("Recived request GetAll");

            var projects = _projectAppService.GetAll();
            return Ok(new BaseResponseEntityDTO<IEnumerable<ProjectDTO>>(_mapper.Map<IEnumerable<ProjectDTO>>(projects)));
        }
        [HttpGet]
        [Route("{projectId}")]
        public ActionResult<BaseResponseEntityDTO<Project>> GetById(int ProjectId)
        {
            Log.Information("Recived request SelectById: {@Id}", ProjectId);

            var project = _projectAppService.GetById(ProjectId);
            return Ok(new BaseResponseEntityDTO<Project>(project));
        }

        [HttpPost]
        public ActionResult<BaseResponseEntityDTO<ProjectDTO>> Insert(ProjectDTO projectDTO)
        {
            Log.Information("Recived request Insert: {@project}", projectDTO);
            var project = _mapper.Map<Project>(projectDTO);

            project.Id = _projectAppService.Insert(project);
            return StatusCode(StatusCodes.Status201Created, new BaseResponseEntityDTO<ProjectDTO>(_mapper.Map<ProjectDTO>(project)));
        }

        [HttpPut]
        public IActionResult Update(ProjectDTO projectDTO)
        {
            Log.Information("Recived request Update: {@project}", projectDTO);
            var project = _mapper.Map<Project>(projectDTO);

            bool sucess = _projectAppService.Update(project);
            if (sucess)
                return StatusCode(StatusCodes.Status200OK);
            else
                return StatusCode(StatusCodes.Status500InternalServerError);
        }
        [HttpGet]
        [Route("getUsers/{projectId}")]
        public ActionResult<BaseResponseEntityDTO<IEnumerable<int>>> GetUsers(int projectId)
        {
            Log.Information("Recived request GetUsers {ProjectId}", projectId);

            var users = _projectAppService.GetUsersOnProject(projectId);
            return Ok(new BaseResponseEntityDTO<IEnumerable<int>>(users));
        }

        [HttpGet]
        [Route("getSystems/{projectId}")]
        public ActionResult<BaseResponseEntityDTO<IEnumerable<int>>> GetSystems(int projectId)
        {
            Log.Information("Recived request GetSystem {ProjectId}", projectId);

            var systems = _projectAppService.GetSystemsOnProject(projectId);
            return Ok(new BaseResponseEntityDTO<IEnumerable<int>>(systems));
        }
        [HttpDelete]
        [Route("{projectId}")]
        public ActionResult<BaseResponseEntityDTO<CaseResponseDTO>> Delete(int projectId)
        {
            Log.Information("Recived request Delete: {@Id}", projectId);
            var result = _projectAppService.UpdateStatus(projectId, Domain.Enum.StatusTypeEnum.Deletado);

            return Ok(new BaseResponseEntityDTO<bool>(result));
        }
    }
}