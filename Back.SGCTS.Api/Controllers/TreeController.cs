﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using Back.SGCTS.ApplicationService.Interfaces;
using Back.SGCTS.DTO.Base;
using Back.SGCTS.DTO.Tree;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;

namespace Back.SGCTS.Api.Controllers
{
    [Authorize]
    [ApiController]
    [Route("api/[controller]")]
    public class TreeController : BaseControler
    {
        private readonly ITreeApplicationService _treeApplicationService;
        private readonly IMapper _mapper;
        public TreeController(ITreeApplicationService treeApplicationService,
                              IMapper mapper,
                              IHttpContextAccessor httpContextAccessor) : base(httpContextAccessor)
        {
            _treeApplicationService = treeApplicationService;
            _mapper = mapper;
        }
        [HttpGet]
        public ActionResult<BaseResponseEntityDTO<IEnumerable<TreeBaseDTO>>> Get ()
        {
            var userId = GetUserId();
            var tree = _mapper.Map<IEnumerable<TreeBaseDTO>>(_treeApplicationService.Get(userId));
            var json = JsonConvert.SerializeObject(new BaseResponseEntityDTO<IEnumerable<TreeBaseDTO>>(tree),
                                                   new JsonSerializerSettings()
                                                   {
                                                       DefaultValueHandling = DefaultValueHandling.Ignore,
                                                       NullValueHandling = NullValueHandling.Ignore
                                                   });
            return Ok(json);
        }
    }
}