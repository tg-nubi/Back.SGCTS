﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using Back.SGCTS.Domain.Domain;
using Back.SGCTS.Domain.Interface.Services;
using Back.SGCTS.DTO.Base;
using Back.SGCTS.DTO.DTO;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace Back.SGCTS.Api.Controllers
{
    [Authorize]
    [ApiController]
    [Route("api/[controller]")]
    [Produces("application/json")]
    public class TrashController : BaseControler
    {
        private readonly ITrashAppService _trashAppService;
        private readonly IMapper _mapper;
        public TrashController(IHttpContextAccessor contextAccessor,
                               ITrashAppService trashAppService, 
                               IMapper mapper) : base(contextAccessor)
        {
            _trashAppService = trashAppService;
            _mapper = mapper;
        }
        [HttpGet]
        public ActionResult<BaseResponseEntityDTO<IEnumerable<TrashDTO>>> Get()
        {
            var trash = _trashAppService.GetAll();
            return Ok(new BaseResponseEntityDTO<IEnumerable<TrashDTO>>(_mapper.Map<IEnumerable<TrashDTO>>(trash)));
        }

        [HttpPost]
        [Route("Recovery")]
        public ActionResult<BaseResponseDTO> Recovery ([FromBody] TrashDTO dto)
        {
            _trashAppService.Recovery(_mapper.Map<Trash>(dto));
            return Ok(new BaseResponseDTO());
        }

    }
}