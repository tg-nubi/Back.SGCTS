﻿using AutoMapper;
using Back.SGCTS.Domain.Domain;
using Back.SGCTS.Domain.Interface.Services;
using Back.SGCTS.DTO.Base;
using Back.SGCTS.DTO.DTO;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Serilog;
using System.Collections.Generic;

namespace Back.SGCTS.Api.Controllers
{
    [ApiController]
    [Authorize]
    [Route("api/[controller]")]
    [Produces("application/json")]
    public class CaseController : BaseControler
    {
        private readonly IMapper _mapper;
        private readonly ICaseAppService _caseAppService;
        private readonly IScenarioAppService _scenarioAppService;
        public CaseController(IMapper mapper,
                              ICaseAppService CaseAppService,
                              IScenarioAppService scenarioAppService,
                              IHttpContextAccessor httpContextAccessor) : base(httpContextAccessor)
        {
            _mapper = mapper;
            _caseAppService = CaseAppService;
            _scenarioAppService = scenarioAppService;
        }

        [HttpGet]
        [Route("{caseId}")]
        public ActionResult<BaseResponseEntityDTO<CaseResponseDTO>> GetById(int caseId)
        {
            Log.Information("Recived request SelectById: {@Id}", caseId);
            var entityCase = _caseAppService.GetById(caseId);
            entityCase.Scenario = _scenarioAppService.GetById(entityCase.ScenarioId);
            
            return Ok(new BaseResponseEntityDTO<CaseResponseDTO>(_mapper.Map<CaseResponseDTO>(entityCase)));
        }

        [HttpGet]
        [Route("user")]
        public ActionResult<BaseResponseEntityDTO<IEnumerable<CaseDTO>>> GetByUserId()
        {
            var userId = GetUserId();

            Log.Information("Recived request SelectByUserId: {@User}", userId);

            var cases = _caseAppService.GetByUserId(userId);
            return Ok(new BaseResponseEntityDTO<IEnumerable<CaseDTO>>(_mapper.Map<IEnumerable<CaseDTO>>(cases)));
        }

        [HttpPost]
        public ActionResult<BaseResponseEntityDTO<CaseDTO>> Insert(CaseDTO caseDTO)
        {
            Log.Information("Recived request Insert: {@case}", caseDTO);
            var cases = _mapper.Map<Case>(caseDTO);

            caseDTO.Id  = _caseAppService.Insert(cases);
            return StatusCode(StatusCodes.Status201Created, new BaseResponseEntityDTO<CaseDTO>(caseDTO));
        }

        [HttpPut]
        public IActionResult Update(CaseDTO caseDTO)
        {
            Log.Information("Recived request Update: {@case}", caseDTO);
            var cases = _mapper.Map<Case>(caseDTO);

            bool sucess = _caseAppService.Update(cases);
            if (sucess)
                return StatusCode(StatusCodes.Status200OK);
            else
                return StatusCode(StatusCodes.Status500InternalServerError);
        }

        [HttpDelete]
        [Route("{caseId}")]
        public ActionResult<BaseResponseEntityDTO<CaseResponseDTO>> Delete(int caseId)
        {
            Log.Information("Recived request Delete: {@Id}", caseId);
            var result = _caseAppService.UpdateStatus(caseId, Domain.Enum.StatusTypeEnum.Deletado);

            return Ok(new BaseResponseEntityDTO<bool>(result));
        }
    }
}
