﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Serilog;

namespace Back.SGCTS.Api.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class UpController : ControllerBase
    {
        [HttpGet]
        public IActionResult Up()
        {
            Log.Information("It's Work");
            return Ok("It's Work");
        }
    }
}