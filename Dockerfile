FROM mcr.microsoft.com/dotnet/core/aspnet:2.2-stretch-slim AS base
RUN apt-get update && apt-get install -y apt-utils libgdiplus libc6-dev
WORKDIR /app
EXPOSE 8080
EXPOSE 443

FROM mcr.microsoft.com/dotnet/core/sdk:2.2-stretch AS build
WORKDIR /src
COPY ["Back.SGCTS.Api/Back.SGCTS.Api.csproj", "Back.SGCTS.Api/"]
RUN dotnet restore "Back.SGCTS.Api/Back.SGCTS.Api.csproj"
COPY . .
WORKDIR "/src/Back.SGCTS.Api"
RUN dotnet build "Back.SGCTS.Api.csproj" -c Release -o /app

FROM build AS publish
RUN dotnet publish "Back.SGCTS.Api.csproj" -c Release -o /app

FROM base AS final
WORKDIR /app
COPY --from=publish /app .


RUN useradd -m myuser
USER myuser
CMD ASPNETCORE_URLS=http://*:$PORT dotnet Back.SGCTS.Api.dll
