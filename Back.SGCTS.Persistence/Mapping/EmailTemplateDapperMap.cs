﻿using Back.SGCTS.Domain.ValueObjects;
using Dapper.FluentMap.Dommel.Mapping;
using System;
using System.Collections.Generic;
using System.Text;

namespace Back.SGCTS.Persistence.Mapping
{
    public class EmailTemplateDapperMap: DommelEntityMap<EmailTemplate>
    {
        public EmailTemplateDapperMap()
        {
            ToTable("EMAIL_TEMPLATE");

            Map(p => p.Id)
                    .ToColumn("PK_EMAIL_TEMPLATE_ID", caseSensitive: false)
                    .IsKey();

            Map(p => p.Subject)
                      .ToColumn("SUBJECT", caseSensitive: false);

            Map(p => p.Body)
                      .ToColumn("BODY", caseSensitive: false);
        }
    }
}
