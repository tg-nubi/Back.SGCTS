﻿using System;
using System.Collections.Generic;
using System.Text;
using Back.SGCTS.Domain.Domain;
using Dapper.FluentMap.Dommel.Mapping;

namespace Back.SGCTS.Persistence.Mapping
{
    class ScenarioDapperMap : DommelEntityMap<Scenario>
    {
        public ScenarioDapperMap()
        {
            ToTable("SCENARIO");

            Map(p => p.Id)
               .ToColumn("PK_SCENARIO_ID", caseSensitive: false)
               .IsKey()
               .IsIdentity();

            Map(p => p.Name)
                .ToColumn("NAME_SCENARIO", caseSensitive: false);

            Map(p => p.Description)
                .ToColumn("DESCRIPTION_SCENARIO", caseSensitive: false);

            Map(p => p.StatusTypeId)
                .ToColumn("FK_STATUS_TYPE_SCENARIO_ID", caseSensitive: false);

            Map(p => p.StatusType)
                .Ignore();

            Map(p => p.ProjectId)
                .ToColumn("FK_PROJECT_SCENARIO_ID", caseSensitive: false);

            Map(p => p.Project)
                .Ignore();
        }
    }
}
