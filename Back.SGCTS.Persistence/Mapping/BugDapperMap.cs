﻿using Back.SGCTS.Domain.Domain;
using Dapper.FluentMap.Dommel.Mapping;
using System;
using System.Collections.Generic;
using System.Text;

namespace Back.SGCTS.Persistence.Mapping
{
    class BugDapperMap : DommelEntityMap<Bug>
    {
        public BugDapperMap()
        {
            ToTable("BUG");

            Map(p => p.Id)
                     .ToColumn("PK_BUG_ID", caseSensitive: false)
                     .IsKey()
                     .IsIdentity();

            Map(p => p.Description)
                      .ToColumn("DESCRIPTION_BUG", caseSensitive: false);

            Map(p => p.Title)
                      .ToColumn("TITLE_BUG", caseSensitive: false);

            Map(p => p.Severity)
                      .ToColumn("FK_SEVERITY_TYPE_BUG_ID", caseSensitive: false);
            
            Map(p => p.StatusTypeId)
                      .ToColumn("FK_STATUS_TYPE_BUG_ID", caseSensitive: false);
            
            Map(p => p.StatusType).Ignore();

            Map(p => p.ActionStepBug)
                      .ToColumn("ACTION_STEP_BUG", caseSensitive: false);

            Map(p => p.Keywords)
                      .ToColumn("KEYWORD_BUG", caseSensitive: false);

            Map(p => p.PriorityId)
                      .ToColumn("FK_PRIORITY_TYPE_BUG_ID", caseSensitive: false);

            Map(p => p.Priority).Ignore();
            
            Map(p => p.ExecutionId)
                      .ToColumn("FK_EXECUTION_BUG_ID", caseSensitive: false);
            
            Map(p => p.Attachment)
                      .ToColumn("ATTACHMENT_BUG", caseSensitive: false);
                      
            Map(p => p.UserId)
                      .ToColumn("FK_USER_BUG_ID", caseSensitive: false);
        }
    }
}
