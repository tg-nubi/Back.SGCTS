﻿using Back.SGCTS.Domain.Domain;
using Dapper.FluentMap.Dommel.Mapping;
using System;
using System.Collections.Generic;
using System.Text;

namespace Back.SGCTS.Persistence.Mapping
{
    class UserDapperMap : DommelEntityMap<User>
    {
        public UserDapperMap()
        {
            ToTable("USERS");

            Map(p => p.Id)
                      .ToColumn("PK_USERS_ID", caseSensitive: false)
                      .IsKey()
                      .IsIdentity();

            Map(p => p.PermissionTypeId)
                      .ToColumn("FK_TYPE_PERMISSION_ID", caseSensitive: false);

            Map(p => p.StatusTypeId)
                      .ToColumn("FK_STATUS_TYPE_ID", caseSensitive: false);

            Map(p => p.Name)
                      .ToColumn("NAME_USERS", caseSensitive: false);

            Map(p => p.LastName)
                      .ToColumn("LAST_NAME_USERS", caseSensitive: false);

            Map(p => p.Email)
                      .ToColumn("EMAIL_USERS", caseSensitive: false);

            Map(p => p.Login)
                      .ToColumn("LOGIN_USERS", caseSensitive: false);

            Map(p => p.Password)
                      .ToColumn("PASSWORD_USERS", caseSensitive: false);

            Map(p => p.ChangePassword)
                      .ToColumn("CHANGE_PASSWORD", caseSensitive: false);

            Map(p => p.Registration)
                      .ToColumn("REGISTRATION", caseSensitive: false);

            Map(p => p.PermissionType)
                .Ignore();

            Map(p => p.StatusType)
                .Ignore();
        }
    }
}
