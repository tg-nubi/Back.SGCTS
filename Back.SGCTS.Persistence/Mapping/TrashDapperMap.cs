﻿using Back.SGCTS.Domain.Domain;
using Dapper.FluentMap.Dommel.Mapping;
using System;
using System.Collections.Generic;
using System.Text;

namespace Back.SGCTS.Persistence.Mapping
{
    public class TrashDapperMap : DommelEntityMap<Trash>
    {
        public TrashDapperMap()
        {
            ToTable("TRASH");

            Map(p => p.Id)
                      .ToColumn("PK_TRASH_ID", caseSensitive: false)
                      .IsIdentity()
                      .IsKey();

            Map(p => p.ObjectId)
                      .ToColumn("OBJECT_ID", caseSensitive: false);

            Map(p => p.Name)
                      .ToColumn("OBJECT_NAME", caseSensitive: false);

            Map(p => p.Type)
                      .ToColumn("OBJECT_TYPE", caseSensitive: false);

            Map(p => p.UserName)
                      .ToColumn("USER_NAME", caseSensitive: false);
        }
    }
}
