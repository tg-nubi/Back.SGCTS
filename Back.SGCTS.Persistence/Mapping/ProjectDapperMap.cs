﻿using Back.SGCTS.Domain.Domain;
using Dapper.FluentMap.Dommel.Mapping;
using System;
using System.Collections.Generic;
using System.Text;

namespace Back.SGCTS.Persistence.Mapping
{
    class ProjectDapperMap : DommelEntityMap<Project>
    {
        public ProjectDapperMap()
        {
            ToTable("PROJECT");

            Map(p => p.Id)
                      .ToColumn("PK_PROJECT_ID", caseSensitive: false)
                      .IsKey()
                      .IsIdentity();

            Map(p => p.Name)
                      .ToColumn("NAME_PROJECT", caseSensitive: false);

            Map(p => p.Goal)
                      .ToColumn("GOAL_PROJECT", caseSensitive: false);

            Map(p => p.StatusTypeId)
                      .ToColumn("FK_STATUS_TYPE_PROJECT_ID", caseSensitive: false);

            Map(p => p.StatusType).Ignore();
        }
    }
}
