﻿using Back.SGCTS.Domain.Domain;
using Dapper.FluentMap.Dommel.Mapping;
using System;
using System.Collections.Generic;
using System.Text;

namespace Back.SGCTS.Persistence.Mapping
{
    class SystemDapperMap : DommelEntityMap<Back.SGCTS.Domain.Domain.System>
    {
        public SystemDapperMap()
        {
            ToTable("SYSTEM");

            Map(p => p.Id)
                .ToColumn("PK_SYSTEM_ID", caseSensitive: false)
                .IsKey()
                .IsIdentity();

            Map(p => p.Name)
                .ToColumn("NAME_SYSTEM", caseSensitive: false);

            Map(p => p.Initials)
                .ToColumn("INITIALS_SYSTEM", caseSensitive: false);

            Map(p => p.StatusTypeId)
                .ToColumn("FK_STATUS_TYPE_SYSTEM_ID", caseSensitive: false);

            Map(p => p.StatusType)
                .Ignore();
        }

    }
}
