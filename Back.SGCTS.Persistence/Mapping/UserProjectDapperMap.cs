﻿using Back.SGCTS.Domain.ValueObjects;
using Dapper.FluentMap.Dommel.Mapping;
using System;
using System.Collections.Generic;
using System.Text;

namespace Back.SGCTS.Persistence.Mapping
{
    public class UserProjectDapperMap : DommelEntityMap<UserProject>
    {
        public UserProjectDapperMap()
        {
            ToTable("USERS_PROJECT");

            Map(p => p.ProjectId)
                .ToColumn("FK_PROJECT_USERS_PROJECT_ID", caseSensitive: false).IsKey();

            Map(p => p.UserId)
                .ToColumn("FK_USERS_ID", caseSensitive: false);

        }
    }
}
