﻿using Back.SGCTS.Domain.ValueObjects;
using Dapper.FluentMap.Dommel.Mapping;
using System;
using System.Collections.Generic;
using System.Text;

namespace Back.SGCTS.Persistence.Mapping
{
    public class SystemProjectDapperMap : DommelEntityMap<SystemProject>
    {
        public SystemProjectDapperMap()
        {
            ToTable("PROJECT_SYSTEM");

            Map(p => p.ProjectId)
                .ToColumn("FK_PROJECT_ID", caseSensitive: false).IsKey();

            Map(p => p.SystemId)
                .ToColumn("FK_SYSTEM_ID", caseSensitive: false);
        }
    }
}
