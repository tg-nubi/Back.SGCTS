﻿using Back.SGCTS.Domain.ValueObjects;
using Dapper.FluentMap.Dommel.Mapping;
using System;
using System.Collections.Generic;
using System.Text;

namespace Back.SGCTS.Persistence.Mapping
{
    public class MenuDapperMap : DommelEntityMap<Menu>
    {
        public MenuDapperMap()
        {
            ToTable("MENU");

            Map(p => p.Id)
                      .ToColumn("PK_MENU_ID", caseSensitive: false)
                      .IsKey();

            Map(p => p.Title)
                      .ToColumn("TITLE", caseSensitive: false);

            Map(p => p.Icon)
                      .ToColumn("ICON", caseSensitive: false);

            Map(p => p.Endpoint)
                      .ToColumn("ENDPOINT", caseSensitive: false);
        }
    }
}
