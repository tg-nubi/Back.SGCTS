﻿using Back.SGCTS.Domain.Domain;
using Dapper.FluentMap.Dommel.Mapping;
using System;
using System.Collections.Generic;
using System.Text;

namespace Back.SGCTS.Persistence.Mapping
{
    public class ExecutionDapperMap : DommelEntityMap<Execution>
    {
        public ExecutionDapperMap()
        {
            ToTable("EXECUTION");

            Map(p => p.Id)
                      .ToColumn("PK_EXECUTION_ID", caseSensitive: false)
                      .IsKey()
                      .IsIdentity();

            Map(p => p.CaseId)
                      .ToColumn("FK_CASES_EXECUTION_ID", caseSensitive: false);

            Map(p => p.UserId)
                      .ToColumn("FK_USERS_EXECUTION_ID", caseSensitive: false);

            Map(p => p.StatusTypeId)
                      .ToColumn("FK_STATUS_TYPE_EXECUTION_ID", caseSensitive: false);

            Map(p => p.Note)
                      .ToColumn("NOTE_EXECUTION", caseSensitive: false);

            Map(p => p.Attachment)
                      .ToColumn("ATTACHMENT_EXECUTION", caseSensitive: false);

            Map(p => p.DateStart)
                      .ToColumn("DATE_START_EXECUTION", caseSensitive: false);

            Map(p => p.DateEnd)
                      .ToColumn("DATE_END_EXECUTION", caseSensitive: false);

            Map(p => p.StatusType)
                      .Ignore();
        }
    }
}
