﻿using Back.SGCTS.Domain.Domain;
using Dapper.FluentMap.Dommel.Mapping;
using System;
using System.Collections.Generic;
using System.Text;

namespace Back.SGCTS.Persistence.Mapping
{
    class CaseDapperMap : DommelEntityMap<Case>
    {
        public CaseDapperMap()
        {
            ToTable("CASES");

            Map(p => p.Id)
                .ToColumn("PK_CASES_ID", caseSensitive: false)
                .IsKey()
                .IsIdentity();

            Map(p => p.Name)
                .ToColumn("NAME_CASES", caseSensitive: false);

            Map(p => p.PreCondition)
                .ToColumn("PRECONDITION_CASES", caseSensitive: false);

            Map(p => p.DataMass)
                .ToColumn("DATAMASS_CASES", caseSensitive: false);

            Map(p => p.ResultExpected)
                .ToColumn("RESULT_EXPECTED_CASES", caseSensitive: false);

            Map(p => p.ScenarioId)
                .ToColumn("FK_SCENARIO_ID", caseSensitive: false);

            Map(p => p.Scenario)
                .Ignore();

            Map(p => p.StatusTypeId)
                .ToColumn("FK_STATUS_TYPE_CASES_ID", caseSensitive: false);

            Map(p => p.StatusType)
                .Ignore();

            Map(p => p.UserId)
                .ToColumn("FK_USERS_CASES_ID", caseSensitive: false);

            Map(p => p.User)
                .Ignore();

            Map(p => p.ReasonBlock)
                .ToColumn("REASON_BLOCK");
        }
    }
}
