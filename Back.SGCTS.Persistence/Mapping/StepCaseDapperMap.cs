﻿using System;
using System.Collections.Generic;
using System.Text;
using Back.SGCTS.Domain.Domain;
using Dapper.FluentMap.Dommel.Mapping;

namespace Back.SGCTS.Persistence.Mapping
{
    class StepCaseDapperMap : DommelEntityMap<StepCase>
    {
        public StepCaseDapperMap()
        {
            ToTable("STEP_CT");

            Map(p => p.Id)
                .ToColumn("PK_STEP_CT_ID", caseSensitive: false)
                .IsKey()
                .IsIdentity();

            Map(p => p.NumberStep)
                .ToColumn("NUMBER_STEP", caseSensitive: false);

            Map(p => p.Action)
                .ToColumn("ACTION_STEP", caseSensitive: false);

            Map(p => p.ResultExpected)
                .ToColumn("RESULT_EXPECTED_STEP", caseSensitive: false);

            Map(p => p.CaseId)
                .ToColumn("FK_CASES_STEP_ID", caseSensitive: false);

            Map(p => p.Case)
                .Ignore();
        }
    }
}
