﻿using Back.SGCTS.Domain.ValueObjects;
using Dapper.FluentMap.Dommel.Mapping;
using System;
using System.Collections.Generic;
using System.Text;

namespace Back.SGCTS.Persistence.Mapping
{
    class SeverityTypeDapperMap : DommelEntityMap<SeverityType>
    {
        public SeverityTypeDapperMap()
        {
            ToTable("SEVERITY_TYPE");

            Map(p => p.Id)
                      .ToColumn("PK_SEVERITY_TYPE_ID", caseSensitive: false)
                      .IsKey()
                      .IsIdentity();

            Map(p => p.Type)
                      .ToColumn("SEVERITY_TYPE", caseSensitive: false);
        }
    }
}
