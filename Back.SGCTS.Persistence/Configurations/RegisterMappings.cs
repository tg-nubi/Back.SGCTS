﻿using Back.SGCTS.Persistence.Mapping;
using Dapper.FluentMap;
using Dapper.FluentMap.Dommel;
using System;
using System.Collections.Generic;
using System.Text;

namespace Back.SGCTS.Persistence.Configurations
{
    public class RegisterMappings
    {
        public static void Register()
        {
            FluentMapper.Initialize(config =>
            {
                config.AddMap(new UserDapperMap());
                config.AddMap(new ProjectDapperMap());
                config.AddMap(new BugDapperMap());
                config.AddMap(new SystemDapperMap());
                config.AddMap(new CaseDapperMap());
                config.AddMap(new ScenarioDapperMap());
                config.AddMap(new ExecutionDapperMap());
                config.AddMap(new StepCaseDapperMap());
                config.AddMap(new EmailTemplateDapperMap());
                config.AddMap(new UserProjectDapperMap());
                config.AddMap(new SystemProjectDapperMap());
                config.AddMap(new SeverityTypeDapperMap());
                config.AddMap(new TrashDapperMap());
                config.ForDommel();
            });
           
        }
    }
}
