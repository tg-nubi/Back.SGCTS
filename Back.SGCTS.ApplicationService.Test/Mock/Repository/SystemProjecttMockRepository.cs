﻿using Back.SGCTS.Domain.Domain;
using Back.SGCTS.Domain.Interface.Repository;
using Back.SGCTS.Domain.ValueObjects;
using System;
using System.Collections.Generic;
using System.Text;

namespace Back.SGCTS.ApplicationService.Test.Mock.Repository
{
    public class SystemProjecttMockRepository : ISystemProjectRepository
    {
        public int Delete(SystemProject systemProject)
        {
            throw new NotImplementedException();
        }

        public IEnumerable<SystemProject> GetAll()
        {
            throw new NotImplementedException();
        }

        public SystemProject GetById(int id)
        {
            throw new NotImplementedException();
        }

        public IEnumerable<int> GetCurrentSystems(int id)
        {
            return new List<int> { 1, 2, 3 };
        }

        public IEnumerable<SystemProject> GetSistemsByProjects(IEnumerable<int> projectIds)
        {
            throw new NotImplementedException();
        }

        public object Insert(SystemProject entity)
        {
            throw new NotImplementedException();
        }

        public bool Update(SystemProject entity)
        {
            throw new NotImplementedException();
        }
    }
}
