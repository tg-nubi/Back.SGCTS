﻿using Back.SGCTS.Domain.Interface.Repository;
using Back.SGCTS.Domain.ValueObjects;
using System;
using System.Collections.Generic;
using System.Text;

namespace Back.SGCTS.ApplicationService.Test.Mock.Repository
{
    public class UserProjecttMockRepository : IUserProjectRepository
    {
        public int Delete(UserProject userProject)
        {
            throw new NotImplementedException();
        }

        public IEnumerable<UserProject> GetAll()
        {
            throw new NotImplementedException();
        }

        public IEnumerable<int> GetAllProjectsByUser(int userId)
        {
            throw new NotImplementedException();
        }

        public UserProject GetById(int id)
        {
            throw new NotImplementedException();
        }

        public IEnumerable<int> GetCurrentUsers(int id)
        {
            return new List<int> { 1, 2, 3 };
        }

        public object Insert(UserProject entity)
        {
            throw new NotImplementedException();
        }

        public bool Update(UserProject entity)
        {
            throw new NotImplementedException();
        }
    }
}
