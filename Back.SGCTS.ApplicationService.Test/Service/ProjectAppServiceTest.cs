﻿using Back.SGCTS.ApplicationService.Services;
using Back.SGCTS.ApplicationService.Test.Mock.Repository;
using Back.SGCTS.Domain.Domain;
using Back.SGCTS.Domain.Interface.Services;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.Text;

namespace Back.SGCTS.ApplicationService.Test.Service
{
    [TestClass]
    public class ProjectAppServiceTest
    {
        private readonly IProjectAppService _projectAppService;
        public ProjectAppServiceTest()
        {
            _projectAppService = new ProjectAppService(null, new UserProjecttMockRepository(), new SystemProjecttMockRepository(), null);
        }
        [TestMethod]
        [TestCategory("SetUsers")]
        public void SetUsersWithRemovedOldUsersAndAddNew()
        {
            var project = new Project() { UsersIds = new List<int> { 2, 5, 6, 7 } };

            _projectAppService.SetUsers(project);
        }
    }
}
