﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Back.SGCTS.Domain.ValueObjects
{
    public class SystemProject
    {
        public int SystemId { get; set; }
        
        public int ProjectId { get; set; }
    }
}
