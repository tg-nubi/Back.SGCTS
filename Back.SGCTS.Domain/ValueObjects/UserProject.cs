﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Back.SGCTS.Domain.ValueObjects
{
    public class UserProject
    {
        public int UserId { get; set; }
        public int ProjectId { get; set; }
    }
}
