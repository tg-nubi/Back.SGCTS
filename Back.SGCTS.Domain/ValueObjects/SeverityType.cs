﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Back.SGCTS.Domain.ValueObjects
{
    public class SeverityType
    {
        public int Id { get; set; }
        public string Type { get; set; }
    }
}
