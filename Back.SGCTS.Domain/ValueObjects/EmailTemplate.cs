﻿using Back.SGCTS.Domain.Enum;
using System;
using System.Collections.Generic;
using System.Text;

namespace Back.SGCTS.Domain.ValueObjects
{
    public class EmailTemplate
    {
        public int Id { get; set; }
        public EmailTypeEnum EmailTypeEnum { get { return (EmailTypeEnum)Id; } set { Id = (int)value; } }
        public string Subject { get; set; }
        public string Body { get; set; }
    }
}
