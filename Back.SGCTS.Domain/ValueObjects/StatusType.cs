﻿using System;

namespace Back.SGCTS.Domain.ValueObjects
{
    public class StatusType
    {
        public int Id { get; set; }
        public string Type { get; set; }
    }
}
