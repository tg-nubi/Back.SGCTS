﻿using System.ComponentModel;

namespace Back.SGCTS.Domain.Enum
{
    public enum PermissionTypeEnum
    {
        [Description("Administrador")]
        Administrador = 1,
        [Description("QA")]
        QA = 2,
        [Description("Coordenador")]
        Coordenador = 3,
        [Description("Desenvolvedor")]
        Desenvolvedor  = 4
    };
}