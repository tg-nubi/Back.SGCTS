﻿using System.ComponentModel;

namespace Back.SGCTS.Domain.Enum
{
    public enum StatusTypeEnum
    {
        [Description("Ativo")]
        Ativo = 1,
        [Description("Inativo")]
        Inativo = 2,
        [Description("Em Execução")]
        Em_execucao = 3,
        [Description("Bloqueado")]
        Bloqueado = 4,
        [Description("BUG")]
        BUG = 5,
        [Description("OK")]
        OK = 6,
        [Description("Não Executado")]
        Nao_executado = 7,
        [Description("Deletado")]
        Deletado = 8,
        [Description("Resolvido")]
        Resolvido = 9

    };
}