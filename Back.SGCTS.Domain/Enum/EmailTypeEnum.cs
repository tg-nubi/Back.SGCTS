﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Back.SGCTS.Domain.Enum
{
    public enum EmailTypeEnum
    {
        Welcome = 1,
        ResetPassword = 2
    }
}
