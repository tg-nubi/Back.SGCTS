﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Text;

namespace Back.SGCTS.Domain.Enum
{
    public enum ObjectTypeEnum
    {
        [Description("Sistema")]
        System,
        [Description("Projeto")]
        Project,
        [Description("Cenário")]
        Scenario,
        [Description("Caso")]
        Case
    }
}
