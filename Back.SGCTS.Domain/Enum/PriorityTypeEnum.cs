﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Text;

namespace Back.SGCTS.Domain.Enum
{
    public enum PriorityTypeEnum
    {
        [Description("Critico")]
        Critico = 1,

        [Description("Alta")]
        Alta,
        
        [Description("Média")]
        Média,

        [Description("Baixa")]
        Baixa

    }
}
