﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Back.SGCTS.Domain.Dashboard
{
    public class ExecutionByStatus
    {
        public DateTime ExecutionDate { get; set; }
        public string Status { get; set; }
        public int Count { get; set; }
    }
}
