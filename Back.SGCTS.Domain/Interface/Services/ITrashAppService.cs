﻿using Back.SGCTS.Domain.Domain;
using System;
using System.Collections.Generic;
using System.Text;

namespace Back.SGCTS.Domain.Interface.Services
{
    public interface ITrashAppService : IBaseCrudService<Trash, int>
    {
        void Recovery(Trash trash);
    }
}
