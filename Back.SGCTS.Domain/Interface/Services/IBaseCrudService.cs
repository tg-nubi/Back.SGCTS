﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Back.SGCTS.Domain.Interface.Services
{
    public interface IBaseCrudService<TEntity, TKey>
    {
        TKey Insert(TEntity entity);
        bool Update(TEntity entity);
        TEntity GetById(TKey id);
        IEnumerable<TEntity> GetAll();
    }
}
