﻿using System.Collections.Generic;
using Back.SGCTS.Domain.Domain;
using Back.SGCTS.Domain.Enum;

namespace Back.SGCTS.Domain.Interface.Services
{
    public interface ICaseAppService : IBaseCrudService<Case, int>
    {
        IEnumerable<Case> GetByScenarioId(int id);
        bool UpdateStatus(int id, StatusTypeEnum statusTypeEnum);

        IEnumerable<Case> GetByUserId(int id);
    }
}
