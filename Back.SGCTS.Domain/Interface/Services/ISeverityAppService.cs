﻿using Back.SGCTS.Domain.ValueObjects;
using System;
using System.Collections.Generic;
using System.Text;

namespace Back.SGCTS.Domain.Interface.Services
{
    public interface ISeverityAppService : IBaseCrudService<SeverityType, int>
    {

    }
}
