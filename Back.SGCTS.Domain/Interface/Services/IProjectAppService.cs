﻿using Back.SGCTS.Domain.Domain;
using Back.SGCTS.Domain.Enum;
using System;
using System.Collections.Generic;
using System.Text;

namespace Back.SGCTS.Domain.Interface.Services
{
    public interface IProjectAppService : IBaseCrudService<Project,int>
    {
        void SetUsers(Project project);

        void SetSystems(Project project);
        IEnumerable<int> GetUsersOnProject(int projectId);
        IEnumerable<int> GetSystemsOnProject(int projectId);
        Project GetFullProject(int projectId);
        bool UpdateStatus(int id, StatusTypeEnum statusTypeEnum);
    }
}
