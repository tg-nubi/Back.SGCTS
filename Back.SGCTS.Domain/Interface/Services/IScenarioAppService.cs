﻿using System;
using System.Collections.Generic;
using System.Text;
using Back.SGCTS.Domain.Domain;
using Back.SGCTS.Domain.Enum;

namespace Back.SGCTS.Domain.Interface.Services
{
    public interface IScenarioAppService : IBaseCrudService<Scenario, int>
    {
        IEnumerable<Scenario> GetByProjectId(int projectId);
        IEnumerable<Scenario> GetFullByProjectId(int projectId);
        bool UpdateStatus(int id, StatusTypeEnum statusTypeEnum);
    }
}
