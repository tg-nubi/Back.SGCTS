﻿using Back.SGCTS.Domain.Domain;
using System;
using System.Collections.Generic;
using System.Text;

namespace Back.SGCTS.Domain.Interface.Services
{
    public interface IBugAppService : IBaseCrudService<Bug,int>
    {
        IEnumerable<Bug> GetByUserId(int id);
    }
}
