﻿using Back.SGCTS.Domain.Domain;
using Back.SGCTS.DTO.Base;
using System;
using System.Collections.Generic;
using System.Text;

namespace Back.SGCTS.Domain.Interface.Services
{
    public interface IUserAppService : IBaseCrudService<User, int>
    {
        BaseResponseEntityDTO<string> ResetPassword(string login);
        void CreatePassword(string login, string password);
    }
}
