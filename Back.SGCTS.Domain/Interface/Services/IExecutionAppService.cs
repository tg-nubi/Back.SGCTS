﻿using Back.SGCTS.Domain.Domain;
using Back.SGCTS.DTO.DTO;
using System.Collections.Generic;

namespace Back.SGCTS.Domain.Interface.Services
{
    public interface IExecutionAppService : IBaseCrudService<Execution, int>
    {
        LoadExecutionDTO LoadExecution(int caseId);

        IEnumerable<Execution> GetExecutionByCase(int caseId);

    }
}
