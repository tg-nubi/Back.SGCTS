﻿using Back.SGCTS.Domain.Domain;
using Back.SGCTS.Domain.Enum;
using System;
using System.Collections.Generic;
using System.Text;

namespace Back.SGCTS.Domain.Interface.Services
{
    public interface ISystemAppService : IBaseCrudService<Back.SGCTS.Domain.Domain.System, int>
    {
        bool UpdateStatus(int id, StatusTypeEnum statusTypeEnum);
    }
}
