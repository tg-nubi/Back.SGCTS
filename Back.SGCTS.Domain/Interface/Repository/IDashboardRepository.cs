﻿using Back.SGCTS.Domain.Dashboard;
using System;
using System.Collections.Generic;
using System.Text;

namespace Back.SGCTS.Domain.Interface.Repository
{
    public interface IDashboardRepository
    {
        IDictionary<string, int> GetQuantityOfStatusBySystem(int systemId);
        IEnumerable<ExecutionByStatus> GetExecutionsByStatusFromSystem(int systemId);
    }
}
