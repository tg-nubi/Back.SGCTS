﻿using Back.SGCTS.Domain.Domain;
using System;
using System.Collections.Generic;
using System.Text;

namespace Back.SGCTS.Domain.Interface.Repository
{
    public interface ISystemRepository : IBaseRepository<Back.SGCTS.Domain.Domain.System, int>
    {
    }
}
