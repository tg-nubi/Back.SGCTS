﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Back.SGCTS.Domain.Interface
{
    public interface IBaseRepository<TEntity, TKey>  
        where TEntity : class
    {
        TEntity GetById(TKey id);
        IEnumerable<TEntity> GetAll();
        object Insert(TEntity entity);
        bool Update(TEntity entity);
    }
}
