﻿using Back.SGCTS.Domain.Domain;
using Back.SGCTS.Domain.ValueObjects;
using System;
using System.Collections.Generic;
using System.Text;

namespace Back.SGCTS.Domain.Interface.Repository
{
    public interface ISystemProjectRepository : IBaseRepository<SystemProject, int>
    {
        IEnumerable<int> GetCurrentSystems(int id);
        int Delete(SystemProject systemProject);
        IEnumerable<SystemProject> GetSistemsByProjects(IEnumerable<int> projectIds);
    }
}
