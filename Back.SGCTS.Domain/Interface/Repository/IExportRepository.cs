﻿using Back.SGCTS.Domain.Domain;
using Back.SGCTS.DTO.DTO;
using System;
using System.Collections.Generic;
using System.Text;

namespace Back.SGCTS.Domain.Interface.Repository
{
    public interface IExportRepository : IBaseRepository<StepCase, int>
    {
        IEnumerable<Export> GetExportWithAllParameters(int systemId, int projectId, int scenarioId);
        IEnumerable<Export> GetExportWithSystem(int systemId);
        IEnumerable<Export> GetExportWithProject(int systemId, int projectId);
        IEnumerable<int> VerifyExistentScenario(string nameSystem, string nameProject, string nameScenario);
        IEnumerable<int> VerifyExistentCase(string nameSystem, string nameProject, string nameScenario, string nameCase);
    }
}
