﻿using System;
using System.Collections.Generic;
using System.Text;
using Back.SGCTS.Domain.Domain;

namespace Back.SGCTS.Domain.Interface.Repository
{
    public interface IStepCaseRepository : IBaseRepository<StepCase, int>
    {
        IEnumerable<StepCase> GetByCaseId(int caseId);

        bool Delete(int stepCaseId);
    }
}
