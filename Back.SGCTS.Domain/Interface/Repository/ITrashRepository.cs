﻿using Back.SGCTS.Domain.Domain;
using System;
using System.Collections.Generic;
using System.Text;

namespace Back.SGCTS.Domain.Interface.Repository
{
    public interface ITrashRepository : IBaseRepository<Trash, int>
    {
        bool Delete(Trash trash);
    }
}
