﻿using Back.SGCTS.Domain.ValueObjects;
using System;
using System.Collections.Generic;
using System.Text;

namespace Back.SGCTS.Domain.Interface.Repository
{
    public interface IEmailTemplateRepository : IBaseRepository<EmailTemplate, int>
    {
    }
}
