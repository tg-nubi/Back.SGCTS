﻿using Back.SGCTS.Domain.Domain;
using System;
using System.Collections.Generic;
using System.Text;

namespace Back.SGCTS.Domain.Interface
{
    public interface IUserRepository : IBaseRepository<User,int>
    {
        User GetUser(string user);
        IEnumerable<string> GetExistsLogins(string login);
    }
}
