﻿using Back.SGCTS.Domain.ValueObjects;
using System;
using System.Collections.Generic;
using System.Text;

namespace Back.SGCTS.Domain.Interface.Repository
{
    public interface IUserProjectRepository : IBaseRepository<UserProject, int>
    {
        IEnumerable<int> GetCurrentUsers(int id);
        int Delete(UserProject userProject);
        IEnumerable<int> GetAllProjectsByUser(int userId);
    }
}
