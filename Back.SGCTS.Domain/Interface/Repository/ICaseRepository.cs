﻿using System;
using System.Collections.Generic;
using System.Text;
using Back.SGCTS.Domain.Domain;

namespace Back.SGCTS.Domain.Interface.Repository
{
    public interface ICaseRepository : IBaseRepository<Case, int>
    {
        IEnumerable<Case> GetByScenarioId(int scenarioId);

        IEnumerable<Case> GetByUserId(int userId);
    }
}
