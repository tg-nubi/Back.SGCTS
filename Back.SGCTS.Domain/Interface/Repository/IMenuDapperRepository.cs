﻿using Back.SGCTS.Domain.Enum;
using Back.SGCTS.Domain.ValueObjects;
using System;
using System.Collections.Generic;
using System.Text;

namespace Back.SGCTS.Domain.Interface.Repository
{
    public interface IMenuDapperRepository
    {
        IEnumerable<Menu> GetMenusByPermission(PermissionTypeEnum permissionType);
    }
}
