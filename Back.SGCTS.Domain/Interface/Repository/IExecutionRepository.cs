﻿using Back.SGCTS.Domain.Domain;
using Back.SGCTS.DTO.DTO;
using System.Collections.Generic;

namespace Back.SGCTS.Domain.Interface.Repository
{
    public interface IExecutionRepository : IBaseRepository<Execution,int>
    {
        IEnumerable<Execution> GetExecutionByCase(int caseId);
    }
}
