﻿using Back.SGCTS.Domain.Domain;
using System;
using System.Collections.Generic;
using System.Text;

namespace Back.SGCTS.Domain.Interface.Repository
{
    public interface IBugRepository : IBaseRepository<Bug,int>
    {
        IEnumerable<Bug> GetByUserId(int userId);
    }
}
