﻿using Back.SGCTS.Domain.Domain;
using System;
using System.Collections.Generic;
using System.Text;

namespace Back.SGCTS.Domain.Interface.Repository
{
    public interface IProjectRepository : IBaseRepository<Project,int>
    {
        IEnumerable<Project> GetProjectsBySystemId(int id);
        IEnumerable<int> GetProjectIdByName(string nameProject);
    }
}
