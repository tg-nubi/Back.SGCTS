﻿using System;
using System.Collections.Generic;
using System.Text;
using Back.SGCTS.Domain.Domain;

namespace Back.SGCTS.Domain.Interface.Repository
{
    public interface IScenarioRepository : IBaseRepository<Scenario, int>
    {
        IEnumerable<Scenario> GetByProjectId(int projectId);
    }
}
