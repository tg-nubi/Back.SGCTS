﻿using System;

namespace Back.SGCTS.Domain.Domain
{
    public class LogTransaction
    {
        public int Id { get; set; }
        public DateTime date { get; set; }
        public int UserId { get; set; }
        public User User { get; set; }
        public string Table { get; set; }
        public  string ChangeRequest { get; set; }
    }
}

