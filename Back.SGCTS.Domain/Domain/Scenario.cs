﻿using Back.SGCTS.Domain.ValueObjects;
using System;

using Back.SGCTS.Domain.Enum;
using System.Collections;
using System.Collections.Generic;

namespace Back.SGCTS.Domain.Domain
{
    public class Scenario
    {
        public int Id { get; set; }
        public int ProjectId { get; set; }
        public Project Project { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public int StatusTypeId { get; set; }
        public StatusTypeEnum StatusType { get { return (StatusTypeEnum)StatusTypeId; } set { StatusTypeId = (int)value; } }
        public IEnumerable<Case> Cases { get; set; }
    }
}
