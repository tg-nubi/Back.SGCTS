﻿using System;

namespace Back.SGCTS.Domain.Domain
{
    public class StepCase
    {
        public int Id { get; set; }
        public int CaseId { get; set; }
        public Case Case { get; set; }
        public int NumberStep { get; set; }
        public string Action { get; set; }
        public string ResultExpected { get; set; }
    }
}
