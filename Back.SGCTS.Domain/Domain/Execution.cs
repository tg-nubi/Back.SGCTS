﻿using Back.SGCTS.Domain.Enum;
using System;
using System.Collections.Generic;
using System.Text;

namespace Back.SGCTS.Domain.Domain
{
    public class Execution
    {
        public int Id { get; set; }
        public int UserId { get; set; }
        public User User { get; set; }
        public int CaseId { get; set; }
        public int StatusTypeId { get; set; }
        public StatusTypeEnum StatusType { get { return (StatusTypeEnum)StatusTypeId;  } set { StatusTypeId = (int)value; } } 
        public string Note { get; set; }
        public string Attachment { get; set; }
        public DateTime DateStart { get; set; }
        public DateTime DateEnd { get; set; }
    }
}
