﻿using Back.SGCTS.Domain.ValueObjects;
using System;
using Back.SGCTS.Domain.Enum;
using System.Collections.Generic;

namespace Back.SGCTS.Domain.Domain
{
    public class Project
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string Goal { get; set; }
        public int StatusTypeId { get; set; }
        public StatusTypeEnum StatusType { get { return (StatusTypeEnum)StatusTypeId; } set { StatusTypeId = (int)value; } }
        public IList<int> UsersIds { get; set; }
        public IList<int> SystemsIds { get; set; }
        public IEnumerable<Scenario> Scenarios { get; set; }
    }
}
