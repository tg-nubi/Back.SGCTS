﻿using Back.SGCTS.Domain.Enum;
using System;
using System.Collections.Generic;
using System.Text;

namespace Back.SGCTS.Domain.Domain
{
    public class Trash
    {
        public int Id { get; set; }
        public ObjectTypeEnum Type { get; set; }
        public int ObjectId { get; set; }
        public string Name { get; set; }
        public string UserName { get; set; }
    }
}
