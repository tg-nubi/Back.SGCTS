﻿using Back.SGCTS.Domain.Enum;
using Back.SGCTS.Domain.ValueObjects;
using System;
using System.Collections.Generic;
using System.Text;

namespace Back.SGCTS.Domain.Domain
{
    public class User
    {
        public int Id { get; set; }
        public int PermissionTypeId { get; set; }
        public PermissionTypeEnum PermissionType { get { return (PermissionTypeEnum)PermissionTypeId; } set { PermissionTypeId = (int)value; } }
        public string Name { get; set; }
        public string LastName { get; set; }
        public string Email { get; set; }
        public string Login { get; set; }
        public string Password { get; set; }
        public bool ChangePassword { get; set; }
        public string Registration { get; set; }
        public int StatusTypeId { get; set; }
        public StatusTypeEnum StatusType { get { return (StatusTypeEnum)StatusTypeId;  } set { StatusTypeId = (int)value; } }

        public string GetHideEmail()
        {
            var splitEmail = Email.Split('@');
            return  string.Concat(HideMid(splitEmail[0]),'@', HideMid(splitEmail[1]));
        }
        public bool IsActive()
        {
            return StatusType == StatusTypeEnum.Ativo;
        }
        private string HideMid(string text)
        {
            var total = text.Length;
            var mid = total / 2;
            text = text.Remove(mid).PadRight(total, '*');

            return text;
        }
    }
}
