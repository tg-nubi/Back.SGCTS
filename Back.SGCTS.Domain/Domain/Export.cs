﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Back.SGCTS.Domain.Domain
{
    public class Export
    {
        public string nameSystem { get; set; }
        public string nameProject { get; set; }
        public string nameScenario { get; set; }
        public string descriptionScenario { get; set; }
        public string nameCase { get; set; }
        public string preconditionCase { get; set; }
        public string datamassCase { get; set; }
        public string resultExpectedCase { get; set; }
        public int numberStep { get; set; }
        public string actionStep { get; set; }
        public string resultExpectedStep { get; set; }
    }
}
