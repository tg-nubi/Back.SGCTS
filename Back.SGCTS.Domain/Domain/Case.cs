﻿using Back.SGCTS.Domain.ValueObjects;
using System;
using Back.SGCTS.Domain.Enum;

namespace Back.SGCTS.Domain.Domain
{
    public class Case
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string PreCondition { get; set; }
        public string DataMass { get; set; }
        public string ResultExpected { get; set; }
        public int ScenarioId { get; set; }
        public Scenario Scenario { get; set; }
        public int StatusTypeId { get; set; }
        public StatusTypeEnum StatusType { get { return (StatusTypeEnum)StatusTypeId; } set { StatusTypeId = (int)value; } }
        public int UserId { get; set; }
        public User User { get; set; }
        public string ReasonBlock { get; set; }
    }
}
