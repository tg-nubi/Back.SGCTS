﻿using System;

namespace Back.SGCTS.Domain.Domain
{
    public class Depedency
    {
        public int CaseDependencyId { get; set; }
        public Case Case { get; set; }
        public int CaseId { get; set; }

    }
}
