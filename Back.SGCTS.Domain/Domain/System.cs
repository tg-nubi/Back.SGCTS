﻿using Back.SGCTS.Domain.ValueObjects;
using System;
using Back.SGCTS.Domain.Enum;
using System.Collections.Generic;

namespace Back.SGCTS.Domain.Domain
{
    public class System
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string Initials { get; set; }
        public int StatusTypeId { get; set; }
        public StatusTypeEnum StatusType { get { return (StatusTypeEnum)StatusTypeId; } set { StatusTypeId = (int)value; } }
        public IEnumerable<Project> Projects { get; set; }
    }
}
