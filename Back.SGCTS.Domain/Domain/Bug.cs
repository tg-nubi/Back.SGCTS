﻿using Back.SGCTS.Domain.ValueObjects;
using System;
using Back.SGCTS.Domain.Enum;

namespace Back.SGCTS.Domain.Domain
{
    public class Bug
    {
        public int Id { get; set; }
        public string Description { get; set; }
        public string Title { get; set; }
        public int Severity { get; set; }
        public string ActionStepBug { get; set; }
        public string Keywords { get; set; }
        public int StatusTypeId { get; set; }
        public StatusTypeEnum StatusType { get { return (StatusTypeEnum)StatusTypeId; } set { StatusTypeId = (int)value; } }
        public int ExecutionId { get; set; }
        public int PriorityId { get; set; }
        public PriorityTypeEnum Priority { get { return (PriorityTypeEnum)PriorityId; } set { PriorityId = (int)value; } }
        public int UserId { get; set; }
        public string Attachment { get; set; }
    }
}
